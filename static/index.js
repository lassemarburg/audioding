var socket = io.connect('http://' + document.domain + ':' + location.port);
var icons = new Icons()

var main = undefined

$(document).ready(() => {
  console.log("document ready")
  main = new Main()
  main.idle()
  //main.showPlaylist({id:"test",tracks:["10-Throw_Away_Your_Television.mp3","Joanne_K._Rowling_-_Harry_Potter_Der_Gefangene_von_Akaban_-__03_.mp3"]})
})

socket.on( 'connect', () => {
  console.log("socket connected to " + 'http://' + document.domain + ':' + location.port)
  
  $(document).ready(() => {
    // $('.modal').modal('hide')
    main.info('Verbindung hergestellt', 'Du kannst auf das Audioding zugreifen.')
    socket.emit('load', {})
  })
})

socket.on( 'connect_error', e => {
  main.disconnected()
})

socket.on( 'new', document => {
  main.newDing(document.id)
  main.info('Leeres Audioding erkannt','')
})

socket.on( 'show', document => {
  console.log("show")
  console.log(document)
  main.showPlaylist(document)
  main.showPlayer()
  main.info('Audioding erkannt','')
  // main.info('Ding erkannt', '')// document.tracks.toString().substring(0, 150) + '...')
})

socket.on( 'empty', status => {
  console.log('idle status')
  console.log(status)
  main.idle(status)
  main.info('Kein Ding platziert', '')
})

socket.on( 'playing', status => {
  main.playStatus(status)
})

socket.on( 'quit', msg => {
  socket.emit('stop')
  main.warn('Server quit running', '')
})

socket.on( 'show_settings', msg => {
  this.main.menu[msg].open()
})

socket.on( 'alert', msg => {
  main.alert(msg.title, msg.message, msg.type, 5000)
})


/**
 * Construct main page
 */
class Main {
  /**
   * add elements to page
   * 
   */
  constructor() {
    this.$ = $(
      '<div id="main" class="px-md-3"></div>'
    )
    
    this.playlist = new Playlist()
    this.player = new Player()
    this.menu = new Menu()
    this.upload = new Upload()
    
    this.$header = $(`
    <nav id="header" class="navbar rounded-bottom navbar-expand-lg navbar-light sticky-top shadow-sm bg-white border">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </nav>`)
    //this.$header = $('<div class="card sticky-top shadow-sm"></div>')
    // <a class="navbar-brand">A</a>
    this.$header.append(this.menu.$settings).prepend(this.menu.$volume).prepend(this.menu.$clear_button).prepend(this.menu.$reload_tracks_button).prepend(this.menu.$add_button)
    
    this.$center = $('<div class=""></div>')
    //container-fluid
    this.$footer = $('<footer id="footer" class="fixed-bottom shadow px-md-3"></footer>')
    
    this.$footer.append(this.player.$)
    
    $('body').append(this.$)
    
    this.$alerts = $('<div id="alerts" class="fixed-bottom"></div>')
    
    this.$.append(this.$header)
    this.$.append(this.$center)
    this.$.append(this.$alerts)
    this.$.append(this.$footer)
  }
  
  clear() {
    this.$center.empty()
    // this.$footer.children().empty()
    this.upload.close()
  }
  
  idle(status) {
    this.clear()
    this.hidePlayer()
    this.menu.$settings.off('closed')
    
    if(status) {
      this.menu.setVolume(status.volume)
    }
    this.menu.disable()
    
    var $idle = $('<div class="pt-5"></div>')
    var $instructions = $(`
      <h2 class="text-center">Stelle ein Ding auf die Box um etwas abzuspielen 
      oder Audiodateien mit einem Ding zu verknüpfen.
      </h2>
    `)
    var $symbol = $('<h1 class="display-1 text-center"></h1>')
    $symbol.append(icons.SOUNDWAVE)
    $symbol.append(icons.SPEAKER)
    
    $idle.append($instructions)
    $idle.append($symbol)
    
    this.$center.append($idle)
  }

  disconnected() {
    this.upload.close()

    this.clear()
    this.hidePlayer()
    this.menu.$settings.off('closed')

    this.menu.disable()

    var $disconnected = $(`
    <div class="py-5 px-4">
      <h2 class="text-center">Verbindung Getrennt</h2>
      <h1 class ="text-center text-danger">${icons.DANGER}</h1>
      <p class="text-center">Es kann keine Verbindung hergestellt werden. Stelle sicher, dass das Audioding eingeschaltet ist und du dich im selben W-Lan befindest.<p>
    </div>
    `)

    main.error('Verbindungsfehler', 'Überprüfe ob das Audioding in Betrieb ist und du im selben W-Lan bist.')
    this.$center.append($disconnected)
  }
  
  showPlayer() {
    //this.$alerts.detach('#alert_spacer')
    $('#alert_spacer').remove()
    this.$alerts.append('<div id="alert_spacer" class="py-5"></div>')
    this.$footer.show(200)
  }
  
  hidePlayer() {
    this.$footer.hide(200)
    $('#alert_spacer').remove()
  }
  
  showMenu() {
    this.$header.show(200)
  }
  
  hideMenu() {
    this.$header.hide(200)
  }
  
  playStatus(status) {
    this.menu.setVolume(status.volume)
    if(status.track && Object.keys(status.track).length != 0) {
      this.player.update(status)
      this.playlist.setActive(status.track, status.pause)
    } else {
      this.player.nofile(status)
      this.playlist.setInactive()
    }
  }
  
  showPlaylist(document) {
    this.clear()
    this.menu.enable(document.options)
    this.playlist.update(document)
    this.$center.append(this.playlist.$)
  }

  trackDisplayName(track) {
    let displayName = ""
    if('artists' in track) {
      let artists = ""
      track.artists.forEach((artist, i) => {
        if(i > 0) {
          artists += " / "
        }

        artists += artist
      })

      displayName += artists + " - "
    }

    if('name' in track) {
      displayName += track.name
    } else if('filename' in track){
      displayName += track['filename']
    }

    if('album' in track) {
      displayName += ` (${track.album})`
    }

    return displayName
  }
  
  newDing(id) {
    if(main.menu.isOpen()) {
      this.menu.$settings.off('closed')
      this.menu.$settings.on('closed', e => {
        this.upload.openNew(id)
      })
    } else {
      this.upload.openNew(id)
    }
  }
  
  addFiles() {
    this.upload.setTitle('Dateien hinzufügen')
    this.upload.setCancelable(true)
    this.upload.setBody('Wähle Audiodateien aus die du zur Playlist hinzufügen willst.')
    
    this.upload.open(this.playlist.id, false)
  }
  
  error(title, msg) {
    this.alert(title, msg, 'danger', 5000)
  }
  
  warn(title, msg) {
    this.alert(title, msg, 'warning', 3500)
  }
  
  info(title, msg) {
    this.alert(title, msg, 'success', 3500)
  }
  
  alert(title, msg, type, duration) {
    var $alert = $(`
      <div class="alert alert-${type} alert-dismissible fade show m-2" role="alert">
        <strong>${title}</strong> ${msg}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
    `)
    $alert.alert()
    this.$alerts.prepend($alert)
    setTimeout(() => {
        $alert.alert('close');
    }, duration)
  }
}

class Menu {
  constructor() {
    this.$add_button = $('<a id="add_button" type="button" title="Dateien hochladen und mit Ding verknüpfen" class="col-auto d-sm-block d-none mx-1 nav-item nav-link border rounded"><div class="row no-gutters justify-content-around"><div class="col-auto mr-1">' + icons.PLUS + '</div><div class="col d-none d-md-block text-truncate">Hinzufügen</div></div></a>')
    this.$reload_tracks_button = $('<a id="reload_tracks_button" type="button" title="Track Liste neu laden" class="col-auto d-sm-block d-none mx-1 nav-item nav-link border rounded"><div class="row no-gutters justify-content-around"><div class="col-auto mr-1">' + icons.RELOAD + '</div><div class="col d-none d-md-block text-truncate">Aktualisieren</div></div></a>')
    this.$clear_button = $('<a id="clear_button" type="button" title="Alle Tracks entfernen" class="col-auto d-sm-block d-none nav-item mx-1 nav-link border rounded"><div class="row no-gutters justify-content-around"><div class="col-auto mr-1">' + icons.TRASH + '</div><div class="col d-none d-md-block text-truncate">Verwerfen</div></div></a>')
    
    this.$mobile_add_button = $('<a id="mobile_add_button" type="button" title="Dateien hochladen und mit Ding verknüpfen" class="d-sm-none nav-item nav-link">' + icons.PLUS + 'Hinzufügen</a>')
    this.$mobile_reload_tracks_button = $('<a id="mobile_reload_tracks_button" type="button" title="Track Liste neu laden" class="d-sm-none nav-item nav-link">' + icons.RELOAD + 'Aktualisieren</a>')
    this.$mobile_clear_button = $('<a id="clear_button" type="button" title="Alle Tracks entfernen" class="d-sm-none nav-item nav-link">' + icons.TRASH + 'Verwerfen</a><div class="dropdown-divider"></div>')
    
    this.$settings = $(`<div class="collapse navbar-collapse" id="navbarNavAltMarkup"><div class="navbar-nav"></div></div>`)

    this.wifi = new WifiSettings()
    this.audio = new AudioSettings()
    this.spotify = new SpotifySettings()
    this.power = new PowerSettings()

    this.wifi.$modal.on('hidden.bs.modal', e => {
      this.$settings.trigger('closed')
    })
    this.audio.$modal.on('hidden.bs.modal', e => {
      this.$settings.trigger('closed')
    })
    this.spotify.$modal.on('hidden.bs.modal', e => {
      this.$settings.trigger('closed')
    })
    this.power.$modal.on('hidden.bs.modal', e => {
      this.$settings.trigger('closed')
    })
    
    this.$ = $('<div class="card-body m-0 p-2 p-md-3 row justify-content-around"></div>')
    this.$volume = $('<div class="col col-lg-4"><div class="row align-items-center"><div class="col-auto p-1 text-right">' + icons.VOL_DOWN + '</div></div></div>')
    this.$slider = $('<input type="range" class="col custom-range" id="volume_slider" min="0" max="1" step="0.01">')
    this.$volume.children('.row').append(this.$slider)
    this.$volume.children('.row').append($('<div class="col-auto p-1 text-left">' + icons.VOL_UP + '</div>'))
    
    this.$settings.children().append(this.$mobile_add_button).append(this.$mobile_reload_tracks_button).append(this.$mobile_clear_button).append(this.wifi.$button).append(this.audio.$button).append(this.spotify.$button).append(this.power.$button)

    this.$add_button.click(e => {
      main.addFiles()
    })
    this.$mobile_add_button.click(e => {
      main.addFiles()
    })

    this.$reload_tracks_button.click(e => {
      socket.emit('reload', {})
    })
    this.$mobile_reload_tracks_button.click(e => {
      socket.emit('reload', {})
    })
    
    this.$clear_button.click(e => {
      main.playlist.clear()
    })
    this.$mobile_clear_button.click(e => {
      main.playlist.clear()
    })
    
    this.$slider.change(e => {
      console.log('change volume ' + parseFloat(this.$slider.val()))
      socket.emit('audio', {'set_volume':parseFloat(this.$slider.val())})
    })
  }

  isOpen() {
    if(this.wifi.isOpen() || this.audio.isOpen() || this.power.isOpen() || this.spotify.isOpen()) {
      console.log("Menu is open")
      return true
    }
    console.log("Menu is closed")
    return false
  }
  
  setVolume(val) {
    console.log('set volume ' + val)
    this.$slider.val(val)
  }
  
  disable() {
    this.$add_button.detach()
    this.$reload_tracks_button.detach()
    this.$clear_button.detach()
    this.$mobile_add_button.detach()
    this.$mobile_reload_tracks_button.detach()
    this.$mobile_clear_button.detach()
  }
  
  enable(options) {
    if(options.includes('clear')) {
      this.$settings.children().prepend(this.$mobile_clear_button)
      $('#header').prepend(this.$clear_button)
    }
    if(options.includes('reload')) {
      this.$settings.children().prepend(this.$mobile_reload_tracks_button)
      $('#header').prepend(this.$reload_tracks_button)
    }
    if(options.includes('add')) {
      this.$settings.children().prepend(this.$mobile_add_button)
      $('#header').prepend(this.$add_button)
    }
    
    /*
    this.$add_button.removeClass('disabled')
    this.$reload_tracks_button.removeClass('disabled')
    this.$clear_button.removeClass('disabled')
    this.$mobile_add_button.removeClass('disabled')
    this.$mobile_reload_tracks_button.removeClass('disabled')
    this.$mobile_clear_button.removeClass('disabled')
    */
  }
  
  disableClear() {
    this.$clear_button.addClass('disabled')
    this.$mobile_clear_button.addClass('disabled')
  }
  
  enableClear() {
    this.$clear_button.removeClass('disabled')
    this.$mobile_clear_button.removeClass('disabled')
  }
}

$.fn.isHScrollable = function () {
    return this[0].scrollWidth > this[0].clientWidth;
};

$.fn.isVScrollable = function () {
  console.log(this[0].scrollHeight + ' > ' + this[0].clientHeight)
    return this[0].scrollHeight > this[0].clientHeight;
};

$.fn.isScrollable = function () {
    return this[0].scrollWidth > this[0].clientWidth || this[0].scrollHeight > this[0].clientHeight;
};
