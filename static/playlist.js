class Playlist {
    constructor() {
      this.$ = $('<ul id="items" class="list-group"></ul>')
      this.id = ""
      this.$.sortable({
        animation: 150,
        removeOnSpill:false,
        revertOnSpill:true,
        delay: 200, // time in milliseconds to define when the sorting should start
          delayOnTouchOnly: true, // only delay if user is using touch
        dataIdAttr:'data-id',
        onUpdate: evt => {
          console.log("Move " + evt.item.title + " from " + evt.oldIndex + " to " + evt.newIndex)
          this.save(this.id, {id: evt.item.id, title:evt.item.title, from: evt.oldIndex, to: evt.newIndex})
        },
        onSpill: evt => {
          // this.remove([evt.item.id], this.id)
        },
        onMove: function (/**Event*/evt) {
                // evt.oldIndex;  // element index within parent
            if ($('html').isVScrollable()) {
              main.hidePlayer()
              // main.hideMenu()
            } 
            
           },
            // Element is unchosen
           onUnchoose: function(/**Event*/evt) {
                 // same properties as onEnd
             main.showPlayer()
             // main.showMenu()
          },
        ghostClass: 'ghost',
        scroll: true, // Enable the plugin. Can be HTMLElement.
          scrollFn: function(offsetX, offsetY, originalEvent, touchEvt, hoverTargetEl) {}, // if you have custom scrollbar scrollFn may be used for autoscrolling
          scrollSensitivity: 30, // px, how near the mouse must be to an edge to start scrolling.
          scrollSpeed: 10, // px, speed of the scrolling
          bubbleScroll: true // apply autoscroll to all parent elements, allowing for easier movement
      })
    }
    
    update(document) {
      this.$.empty()
      Object.assign(this, document)
      
      console.log(`${this.type} playlist update for ${this.name}`)

      this.$.append($('<div class="my-2"></div>'))
      
      if(!this.tracks.length) {
        main.menu.disableClear()
      } else {
        main.menu.enableClear()
      }
      
      for(let track of this.tracks) {
        let track_name = main.trackDisplayName(track)
        let $bar = $('<li id="' + track.id + '" title="' + track_name + '" data-id="' + track.id + '" class="playlist list-group-item list-group-item-action"></li>')
        let $row = $('<div class="row justify-content-end align-items-center"><div class="col-sm pr-0 text-break">' + track_name + '</div></div>')
        let $options = $('<div class="col-sm-auto pl-0 align-self-end"></div>')
        let $play_btn = $('<button id="play_btn" type="button" class="options_icon badge-pill btn btn-outline-secondary">' + icons.PLAY + '</button>')
        let $remove_btn = $('<button id="remove_btn" type="button" class="mr-1 options_icon badge-pill btn btn-outline-secondary">' + icons.X + '</button>')
        
        
        $remove_btn.click(e => {
          this.remove(track, this.id)
        })
        $play_btn.click(e => {
          if($bar.hasClass('active')) {
            if(main.player.pause) {
              console.log('play audio')
              socket.emit('audio',{'playback':'resume'})
              // $play_btn.html(icons.PAUSE)
              // main.player.set_play()
            } else {
              console.log('pause audio')
              socket.emit('audio',{'playback':'pause'})
              // $play_btn.html(icons.PLAY)
              // main.player.set_pause()
            }
          } else {
            console.log('switch track ' + track.name + ' ' + track.id)
            socket.emit('audio', {'switch_track':track.id})
            // main.player.set_play()
          }
        })
        
        $bar.mouseenter(e => {
          $options.children('.btn-outline-secondary').toggleClass('btn-outline-secondary btn-outline-primary')
        })
        $bar.mouseleave(e => {
          $options.children('.btn-outline-primary').toggleClass('btn-outline-secondary btn-outline-primary')
        })
        
        $options.append($play_btn)
        if(document.options.includes('remove')) {
          $options.append($remove_btn)
        }
        $row.append($options)
        $bar.append($row)
        this.$.append($bar)
      }
      this.$.append($('<div class="my-4 py-4"></div>'))
      console.log('id on update: ' + this.id)
    }
    
    setActive(track, pause) {
      console.log("Set Active " + track.name)
      this.setInactive()
      
      if(track.id) {
        let $active = $('li[id="' + track.id + '"]')
        $active.addClass('active')
        $active.find('button').addClass('btn-light').removeClass('btn-outline-secondary').removeClass('btn-outline-primary')
        if(main.player.pause) {
          $active.find('#play_btn').html(icons.PLAY)
        } else {
          $active.find('#play_btn').html(icons.PAUSE)
        }
      }
    }
    
    setInactive() {
      $('.active').find('button').toggleClass('btn-light btn-outline-secondary')
      $('.active').find('#play_btn').html(icons.PLAY)
      
      $('.active').removeClass('active')
    }
    
    set_pause() {
      $('.active').find('#play_btn').html(icons.PLAY)
    }
    
    set_play() {
      $('.active').find('#play_btn').html(icons.PAUSE)
    }
    
    /**
     * save list of tracks the way they are sorted now.
     * @param {String} id - ding tag id
     */
    save(ding_id, reordered_track) {
      console.log("Save track list for " + ding_id)
      var list = this.$.children('li').map(function() {
        return $(this).attr('data-id');
      }).get()
      console.log(list)
      let tracks = list.map(track_id => {
        for(let track of this.tracks)
        {
          if(track.id == track_id) {
            return track
          }
        }
      })
      socket.emit( 'reorder', {id:ding_id, tracks:tracks, track:reordered_track}, response => {
        if(response && response.error) {
          return
        }
        main.info('Track verschoben', reordered_track.title)
      })
    }
    
    remove(track, id) {
      var $confirm_modal = $(
        `<div class="modal fade" id="remove_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Datei entfernen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Willst du <strong>${track.name.toString().replace(/,/g,",\n")}</strong> entfernen?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                <button id="delete_button" type="button" class="btn btn-danger">Entfernen</button>
              </div>
            </div>
          </div>
        </div>
      `)
      
      $confirm_modal.find('#delete_button').click(e => {
        console.log("remove " + track.name + " " + id)
        socket.emit('remove', {"files":[track.filename], "id":id})
        
        /*
        $.each( files, function( i, val ) {
          $( 'li[id="' + val + '"]' ).remove()
        })
        */
        
        $confirm_modal.modal('hide')
      })
      
      $confirm_modal.modal()
    }

    confirmModal(text, title, button) {
      return $(
        `<div class="modal fade" id="remove_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">${title}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                ${text}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                <button id="delete_button" type="button" class="btn btn-danger">${button}</button>
              </div>
            </div>
          </div>
        </div>
      `)
    }
    
    clear() {
      if(this.type == "spotify") {
        let $confirm_modal = this.confirmModal(`<p>Bist du sicher, dass du die spotify playlist <span class="font-weight-bold">${this.name}</span> von diesem Ding entfernen möchstest?</p> 
                                            <p class="font-weight-light">Die playlist bleibt auf spotify erhalten und du kannst sie jederzeit wieder mit einem Ding verknüpfen.</p>`,
                                            `Playlist von Ding entfernen`,
                                            `Entfernen`
                                          )
        $confirm_modal.find('#delete_button').click(e => {
          console.log("Clear " + this.name + " " + this.id)
          socket.emit('clear',{})
          
          $confirm_modal.modal('hide')
        })
        
        $confirm_modal.modal()
      } else {
        let $confirm_modal = this.confirmModal(`<p>Bist du sicher, dass du alle Audiotracks von diesem Ding entfernen möchtest?</p> 
                                            <p class="font-weight-light">Du kannst das Ding anschließend mit anderen files verknüpfen.</p>`,
                                            `Playlist von Ding entfernen`,
                                            `Entfernen`
                                          )
        $confirm_modal.find('#delete_button').click(e => {
          console.log("Clear " + this.name + " " + this.id)
          // socket.emit('remove', {"files":this.tracks.map(track => track.filename), "id":this.id})
          socket.emit('clear',{})

          $confirm_modal.modal('hide')
        })
        
        $confirm_modal.modal()
      }
    }
  } 