class Player {
    constructor() {
      this.play_position
      this.pause = true
      this.dragging = false
      this.$ = $('<div class="card"><div class="row card-body m-0 p-2 p-md-3 justify-content-around align-items-center"></div></div>')
      
      this.$control = $('<div class="col-md-4"><div class="row justify-content-around align-items-center"></div></div>')
      this.$skip_start = $('<button id="skip_start_btn" type="button" class="col-2 p-0 p-md-2 btn btn-outline-primary">' + icons.SKIP_START + '</button>')
      this.$backward = $('<button id="backward_btn" type="button" class="col-2 p-0 p-md-2 btn btn-outline-primary">' + icons.COUNTERCLOCKWISE + '</button>')
      this.$play = $('<button id="play_btn" type="button" class="col-2 p-0 p-md-2 btn btn-outline-primary">' + icons.PLAY + '</button>')
      this.$forward = $('<button id="forward_btn" type="button" class="col-2 p-0 p-md-2 btn btn-outline-primary">' + icons.CLOCKWISE + '</button>')
      this.$skip_end = $('<button id="skip_end_btn" type="button" class="col-2 p-0 p-md-2 btn btn-outline-primary">' + icons.SKIP_END + '</button>')
      
      this.$status = $('<div class="col-md-8"><div class="row pb-1 pb-md-0 justify-content-around align-items-center"></div></div>')
      
      this.$track = $('<div class="col-12 text-primary text-truncate text-center" title="Wiedergabe gestoppt">Wiedergabe gestoppt</div>')
      this.$position = $('<div class="col-3 col-sm-2 col-lg-1 pr-1 pl-0 text-nowrap text-right"><small>00:00:00</small></div>')
      this.$duration = $('<div class="col-3 col-sm-2 col-lg-1 pl-1 pr-0 text-nowrap text-left"><small>00:00:00</small></div>')
      this.$progress = $('<input type="range" class="form-control-range col-6 col-sm-8 col-lg-10" min="0" id="play_progress">')
      
      this.$progress.on('input change', e => {
        this.set_position(this.$progress.val())
      })
      
      this.$progress.on('touchstart mousedown', e => {
        console.log('set progress ' + this.$progress.val())
        this.dragging = true
      })
      
      this.$progress.on('touchend mouseup', e => {
        if(parseFloat(this.$progress.val()) != this.play_position) {
          this.play_position = parseFloat(this.$progress.val())
          socket.emit('audio', {'position':parseFloat(this.play_position)})
        }
        this.dragging = false
      })
      
      this.$skip_start.click(e => {
        socket.emit('audio',{'playback':'skip_start'})
      })
      
      this.$backward.click(e => {
        socket.emit('audio',{'backward':30})
      })
      
      this.$play.click(e => {
        if(this.pause) {
          console.log("resume audio")
          socket.emit('audio',{'playback':'resume'})
          // this.set_play()
          // main.playlist.set_play()
        } else {
          console.log("pause audio")
          socket.emit('audio',{'playback':'pause'})
          // this.set_pause()
          //main.playlist.set_pause()
        }
      })
      
      this.$forward.click(e => {
        socket.emit('audio',{'forward':30})
      })
      
      this.$skip_end.click(e => {
        socket.emit('audio',{'playback':'skip_end'})
      })
      
      this.$control.children().append(this.$skip_start)
      this.$control.children().append(this.$backward)
      this.$control.children().append(this.$play)
      this.$control.children().append(this.$forward)
      this.$control.children().append(this.$skip_end)
      
      this.$status.children().append(this.$track)
      this.$status.children().append(this.$position)
      this.$status.children().append(this.$progress)
      this.$status.children().append(this.$duration)
      
      //this.$.children().append(this.$track)
      this.$.children().append(this.$status)
      this.$.children().append(this.$control)
    }
    
    update(status) {
      console.log(status)
      
      this.play_position = status.position
      
      let len_sec = Math.floor(status.track.duration)
      let pos_sec = Math.floor(status.position)
      
      let track_name = main.trackDisplayName(status.track)
      this.$track.text(track_name)
      this.$track.attr('title', track_name)
      this.$track.removeClass('text-muted')
      
      this.$control.find('button').attr('disabled', false)
      
      this.$duration.children().text(this.formattedTime(len_sec))
      this.set_position(pos_sec)
      
      this.$progress.attr('disabled', false)
      this.$progress.attr('max', len_sec)
      this.$progress.val(pos_sec)
      
      if(status.pause) {
        this.set_pause()
      } else {
        this.set_play()
      }
      
      
      //this.$player.html($('<div class="card-body">' + status.track + ' ' + status.position + ' ' + status.track.duration + '</div>'))
    }
    
    set_position(pos_sec) {
      this.$position.children().text(this.formattedTime(pos_sec))
    }
    
    set_play() {
      this.pause = false
      clearInterval(this.interval)
      this.interval = setInterval(this.timerUpdate.bind(this), 100)
      this.$play.html(icons.PAUSE)
    }
    
    set_pause() {
      this.pause = true
      clearInterval(this.interval)
      this.$play.html(icons.PLAY)
    }
    
    timerUpdate() {
      this.play_position += 0.1
      
      if(!this.dragging) {
        this.$progress.val(this.play_position)
      }
      
      let pos = Math.floor(this.play_position)
      this.$position.children().text(this.formattedTime(pos))
    }
    
    formattedTime(seconds) {
      var date = new Date(null);
      date.setSeconds(seconds); // specify value for SECONDS here
      return date.toISOString().substr(11, 8);
    }
    
    nofile(status) {
      this.$track.addClass('text-muted')
      clearInterval(this.interval)
      this.$position.children().text(this.formattedTime(0))
      this.$duration.children().text(this.formattedTime(0))
      
      this.$control.find('button').attr('disabled', true)
  
      this.$progress.attr('disabled', true)
    }
  }