class Upload extends Modal {
  constructor() {
    super()
    this.id = ''
    this.cancelable = true

    this.$modal = $(`
      <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="staticBackdropLabel">Audiodateien hinzufügen</h5>
              <button id="close_modal_button" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div id="upload_box_body" class="modal-body">
            </div>
            <div class="modal-footer">
              <button id="close_upload_button" type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
              <button id="reset_arrangement_button" type="button" class="btn btn-secondary">Abbrechen</button>
              <button id="upload_button" type="button" class="btn btn-primary">Hochladen</button>
              <button id="arrangement_back_button" type="button" class="btn btn-primary">Zurück</button>
              <button id="arrangement_confirm_button" type="button" class="btn btn-primary">Auswählen</button>
            </div>
          </div>
        </div>
      </div>
    `)
    
    this.$progress_bar = $(`
      <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
      </div>`
    )
    
    this.$body_text = $('<div></div>')
    this.$choose_button = $(`<div class="col p-3 text-center border-right"><button type="button" class="btn btn-primary p-2"><h4 class="text-center">${icons.SOUNDFILE}</h4>Dateien auswählen</button></div>`)

    this.$choose_spotify = $(`<div class="col p-3 text-center"><button type="button" class="btn spotify p-2" ><h6 class="text-center">${icons.SPOTIFY_LOGO}</h6>Playlist auswählen</button></div>`)
    
    this.$choose_file = $('<input type="file" id="file" name="file" multiple="multiple" accept=".mp3,.ogg,.mod,.wav"/>').hide()
    this.$form = $('<form action="http://localhost:5000/upload" method="POST" enctype="multipart/form-data">') // is the action url obsolete? It should be because it can not be correct
    
    
    var data = undefined
    
    this.$file_list = $('<ul id="upload_items" class="list-group"></ul>')

    this.$modal.find('#reset_arrangement_button').click(e => {
      if(this.upload_xhr) {
        console.log('upload XHR ready state: ' + this.upload_xhr.readyState)
        console.log(this.upload_xhr)
        if(this.upload_xhr.readyState) {
          this.upload_xhr.abort()
          return
        }
      }
      this.reset()
    })
  }

  loading(msg="") {
    this.$modal.find('.modal-body').html(`
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" role="status">
            <span class="sr-only"></span>
          </div>
        </div>
        <div class="m-3"><p class="text-center">${msg}</p></div>
      `)
  }
  
  /**
   * Return to opening page or close modal
   */
  reset() {
    if(this.new) {
      this.openNew()
    } else {
      this.close()
    }
  }

  setTitle(title) {
    this.$modal.find('.modal-title').text(title)
  }
  
  setBody(text) {
    this.$body_text.text(text)
  }
  
  setCancelable(cancelable) {
    this.cancelable = cancelable
    if(cancelable) {
      this.$modal.find('[data-dismiss="modal"]').show()
    } else {
      this.$modal.find('[data-dismiss="modal"]').hide()
    }
  }
  
  open(id, is_new_ding) {
    if(typeof id === "undefined") {
      id = this.id
    }
    console.log("open Modal to add files for " + id)
    this.id = id
    this.new = is_new_ding
    this.$modal.find('.modal-footer').children().hide()
    this.$choose_file.hide()
    this.$modal.modal()
    this.$body_text.show()
    this.$modal.find('.modal-body').children().detach()
    
    this.$modal.find('.modal-body').append(this.$body_text)
    //this.$modal.find('.modal-body').append(this.$choose_button)
    this.$options = $(`<div class="row my-3"></div>`)
    this.$options.append(this.$choose_button)
    this.$modal.find('.modal-body').append(this.$options)

    this.$choose_button.off('click')
    this.$choose_button.click(e => {
      this.$choose_file.trigger('click')
    })

    let data

    this.$choose_file.off('change')
    this.$choose_file.change(e => {
      data = this.showSoundfiles(data)
    })
    
    this.$modal.find('#upload_button').off('click')
    this.$modal.find('#upload_button').click(e => {
      this.fileUpload(data)
    })

    this.$form.append(this.$choose_file)
    this.$modal.find('.modal-body').append(this.$form)
  }

  /**
   * open upload modal for new ding.
   * Show file chooser and spotify chooser
   */
  openNew(id) {
    this.new = true
    this.setTitle('Neues Ding')
    this.setCancelable(false)
    this.setBody('Dieses Ding ist noch leer. Füge Audiodateien zur Playlist hinzu indem du sie hochlädst. Oder verknüpfe das Ding mit einer Spotify Playlist.')

    this.open(id, true)
    this.$options.append(this.$choose_spotify)

    this.$choose_spotify.off('click')
    this.$choose_spotify.click(e => {
      this.loadSpotifyPlaylists()
    })
  }

  showSoundfiles(data) {
    console.log(this.$choose_file.prop('files'))
    this.setTitle("Neues Ding - Sounddateien Hochladen")
    
    if(!this.$choose_file.prop('files').length) {return}
    
    this.$modal.find('#upload_button').show()
    this.$choose_spotify.detach()
    this.$modal.find('#reset_arrangement_button').show()
    
    this.$file_list.empty()
    
    data = new FormData();
    $.each($("#file")[0].files, (i, file) => {
        data.append('file-'+i, file)
        this.$file_list.append($('<li id="' + file.name + '" class="list-group-item">' + file.name + '</li>'))
    })
    data.append('id',this.id)
    data.append('new',this.new)
    
    this.$modal.find('.modal-body').prepend(this.$file_list)
    this.$body_text.hide()

    return data
  }

  fileUpload(data) {
    this.$modal.find('#upload_button').hide()
    this.$modal.find('[data-dismiss="modal"]').hide()
    
    this.$modal.find('.modal-body').children().detach()
    this.$modal.find('.modal-body').append(this.$progress_bar)
    this.$modal.find('.modal-body').prepend('<p>Dateien werden hochgeladen</p>')
    
    this.$progress_bar.children().attr('aria-valuenow', 0)
    this.$progress_bar.children().css('width', "0%")
    $.post({
      url:'http://' + document.domain + ':' + location.port + '/upload', 
      data: data,
      contentType:false,
      processData:false,
      xhr: () => {
            this.upload_xhr = $.ajaxSettings.xhr()
            if(this.upload_xhr.upload){
                this.upload_xhr.upload.addEventListener('progress',e => {
                  if(e.lengthComputable){
                      var p = (e.loaded * 100)/e.total
                      console.log('upload ' + p + ' %')
                      this.$progress_bar.children().attr('aria-valuenow', p)
                      this.$progress_bar.children().css('width', p + "%")
                      if(p >= 100)
                      {
                          // process completed  
                      }
                  } 
                }, false);
                this.upload_xhr.upload.addEventListener('abort',e => {
                  console.log('upload canceled')
                  main.warn('Upload unterbrochen', JSON.stringify(e))
                  this.reset()
                })
                this.upload_xhr.upload.addEventListener('error',data => {
                  main.error('Upload fehlgeschlagen. ', JSON.stringify(data))
                  console.error(data)
                  this.reset()
                })
            }
            return this.upload_xhr;
      }
    })
    .then(result => {
      console.log('Upload finished')
      console.log(result)
      main.info('Upload erfolgreich', JSON.stringify(result))
      this.close()
    })
  }

  loadSpotifyPlaylists() {
    console.log("Load Spotify Playlist")
    this.loading()
    socket.emit("spotify",{"playlists":0}, result => {
      console.log(result)
      if(result.error) {
        return this.errorHandling(result.error)
      }
      
      if(result.items) {
        this.spotifyPlaylists(result.items)
      }
    })
  }

  spotifyPlaylists(playlists) {
    this.setTitle("Neues Ding - Spotify Playlist auswählen")
    this.$modal.find('.modal-footer').children().hide()
    this.$modal.find('#reset_arrangement_button').show()
    let $playlists = $('<ul id="playlists" class="list-group"></ul>')
    playlists.forEach(playlist => { // list-group-item d-flex justify-content-between align-items-start
      $playlists.append($(`<li id="${playlist.id}" class="list-group-item list-group-item-action text-break align-items-start d-flex justify-content-between">
        <div class="ms-2 me-auto"><div class="font-weight-bold">${playlist.name}</div> ${playlist.description}</div>
        <span class="badge badge-secondary badge-pill">${playlist.tracks.total} tracks</span>
      </li>`))

      $playlists.children('#' + playlist.id).click(e => {
        this.loading()
        socket.emit('spotify', {"get_playlist":playlist.id}, result => {
          if(result.error) {
            return this.errorHandling(result.error)
          }
          this.showSpotifyPlaylist(playlist, result.tracks.items)
        })
      })
    })
    
    this.$modal.find('.modal-body').html($playlists)
  }

  /**
   * Show playlist preview with list of artists and tracks
   * 
   * @param {Object} playlist - spotify playlist object 
   * @param {Array} tracks - list of first 100 tracks in this playlist
   */
  showSpotifyPlaylist(playlist, tracks) {
    this.setTitle(`Neues Ding - ${playlist.name}`)
    let $playlist = $('<ul id="playlist" class="list-group"></ul>')

    tracks.forEach(track => {
      let artists = ""
      track.track.artists.forEach((artist, i) => {
        if(i > 0) {
          artists += ", "
        }

        artists += artist.name
      })

      $playlist.append($(`<li id="${track.track.id}" class="list-group-item text-break">
      <div class="font-weight-bold">${track.track.name}</div> ${artists} (Album: ${track.track.album.name})
      </li>`))
    })

    this.$modal.find('.modal-footer').children().hide()
    this.$modal.find('#reset_arrangement_button').show()
    this.$modal.find('#arrangement_back_button').show()
    this.$modal.find('#arrangement_confirm_button').show()

    this.$modal.find('#arrangement_confirm_button').off('click')
    this.$modal.find('#arrangement_confirm_button').click(e => {
      this.loading()
      socket.emit('assign', {'id':this.id, 'type':'spotify','playlist':playlist}, res => {
        console.log("Add spotify playlist")
        console.log(res)
        this.close()
      })
    })

    this.$modal.find('#arrangement_back_button').off('click')
    this.$modal.find('#arrangement_back_button').click(e => {
      this.loadSpotifyPlaylists()
    })

    this.$modal.find('.modal-body').html($playlist)
  }

  errorHandling(error) {
    if(error == "not authenticated") {
      this.reset()
      this.hint("error",`Verbindung mit Spotify nicht möglich. Melde dich in deinem account in den <a href="${window.location.origin}/settings/spotify">Spotify Einstellungen</a> an.`)
    } else if(error.type == "ConnectError") {
      this.reset()
      this.hint("error",`Verbindung mit dem Internet nicht möglich. Überprüfe die <a href="${window.location.origin}/settings/wifi">W-Lan Einstellungen</a>.`)
    }
  }
  
  close() {
    this.$modal.modal('hide')
  }
}