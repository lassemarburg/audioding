class Modal {
  constructor() {

  }

  /**
   * Replace modal content with loading spinner and (optional) message
   * 
   * @param {string} [msg=""] - (html) loading description
   */
  loading(msg="") {
    this.$modal.find('.modal-body').html(`
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" role="status">
            <span class="sr-only"></span>
          </div>
        </div>
        <div class="m-3"><p class="text-center">${msg}</p></div>
      `)
  }

  /**
   * Append a hint box to current modal content
   * 
   * @param {string} type - "error" for important hints or "info"
   * @param {string} msg - (html) info or error message to display
   */
  hint(type, msg) {
    if(type == "info") {
      var icon = icons.INFO
      var icon_color = "success"
    } else if(type == "error") {
      var icon = icons.DANGER
      var icon_color = "danger"
    }
    this.$modal.find('.modal-body').append(`
      <div class="bg-light rounded p-2"><p class="m-0">
        <span class="text-${icon_color} mx-1">${icon}</span>
        <span class="align-middle">${msg}</span></p>
      </div>`
    )
  }
}
class Settings extends Modal {
  constructor(name, title, icon) {
    super()
    this.name = name
    this.title = title
    this.$button = $(`<a id="${name}_button" class="nav-item nav-link" type="button">${icon}<span class="align-bottom ml-1">${title}</span></a>`)

    this.$modal = $(`
        <div class="modal fade" id="${this.name}_modal" tabindex="-1" role="dialog" aria-labelledby="${this.name}_modal_label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="${this.name}_modal_label">${this.title}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer"></div>
            </div>
            </div>
        </div>
        `)

    this.$button.click(e => {
      this.open()
    })

    this.$modal.on('hidden.bs.modal', e => {
      window.history.pushState({},"","/")
    })
  }

  open() {
    window.history.pushState({},this.title, "/settings/" + this.name)
    this.$modal.modal()
    this.loading()
    this.edit()
  }

  loading(msg="") {
    this.$modal.find('.modal-body').html(`
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" role="status">
            <span class="sr-only"></span>
          </div>
        </div>
        <div class="m-3"><p class="text-center">${msg}</p></div>
      `)
  }

  close() {
    console.log("Close " + window.origin)
    window.history.pushState({},"", window.origin)
    this.$modal.modal('hide')
  }

  edit() {

  }

  isOpen() {
    return (this.$modal.data('bs.modal') || {})._isShown
  }

  /**
   * Is not being used by any child class.
   * 
   * @todo adapt wifi settings to use this or remove it
   * @param {*} name 
   * @param {*} minlength 
   * @param {*} maxlength 
   */
  appendPasswordForm(name, minlength, maxlength) {
    this.$modal.find(".modal-body").append(`
    <div id="${name}_passwort_settings">
      <form id="${name}_password_form">
        <div class="form-group">
          <label for="${name}_password_input" class="font-weight-bold">Passwort</label>
          <input type="password" class="form-control" id="${name}_password_input" pattern=".{${minlength},${maxlength}}" required>
        </div>
      </form>
      <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="show_hide_${name}_password">
        <label class="form-check-label" for="show_hide_${name}_password">Passwort anzeigen</label>
      </div>
    </div>
    `)

    $(`#show_hide_${name}_password`).click(e => {
      if($(`#show_hide_${name}_password`).is(':checked')) {
        $(`#${name}_password_input`).attr('type', 'text')
      } else {
        $(`#${name}_password_input`).attr('type', 'password')
      }
    })
  }

  /**
   * Is not being used by any child class.
   * 
   * @todo adapt wifi settings to use this or remove it
   * @param {*} name 
   * @param {*} minlength 
   * @param {*} maxlength 
   */
  validatePassword(name, minlength, maxlength) {
    console.log($(`#${name}_password_input`).val())
    $(`#${name}_password_input`).addClass("is-invalid")
    $(`#${name}_password_form`).addClass("was-validated")
    if($(`#${name}_password_input`).val().length >= minlength && $(`#${name}_password_input`).val().length <= maxlength) {
      return $(`#${name}_password_input`).val()
    } else {
      console.error("Password length is not valid.")
      return undefined
    }
  }
}

class PowerSettings extends Settings {
  constructor() {
    super('power', 'Ausschalten', icons.POWER)
    this.$button.prop('title','Herunterfahren oder neu starten')
  }

  edit() {
    this.$modal.find('.modal-body').html(`
      <div class="row m-2">
        <button id="shutdown_button" type="button" class="btn btn-light col mx-2"><h1 class="text-center">${icons.POWER}</h1>Herunterfahren</button>
        <button id="reboot_button" type="button" class="btn btn-light col mx-2"><h1 class="text-center">${icons.COUNTERCLOCKWISE}</h1>Neu starten</button>
      </div>
    `)
    
    this.$modal.find('#shutdown_button').click(e => {
      console.log("shutdown")
      this.loading()
      socket.emit('system', {"shutdown":true}, response => {
        console.log(response)
        if(response.error) {
          main.error("Shutdown Fehler", response.error.type + " " + response.error.message)
          return
        }
        main.info(response.info)
        this.$modal.find('.modal-body').html(`
          <div class="modal-body">
            Wird heruntergefahren ...
          </div>
        `)
        socket.on( 'connect', () => {
          this.close()
        })
      })
    })

    this.$modal.find('#reboot_button').click(e => {
      console.log("reboot")
      this.loading()
      socket.emit('system', {"reboot":true}, response => {
        console.log(response)
        if(response.error) {
          main.error("Reboot Fehler", response.error.type + " " + response.error.message)
          return
        }
        this.loading("Wird neu gestartet ...")
        socket.on( 'connect', () => {
          this.close()
        })
      })
    })
  }
}

class WifiSettings extends Settings{
  constructor() {
    super("wifi", "W-Lan", icons.WIFI)
    this.$button.prop('title','Internet Verbindung einstellen')
  }

  edit() {
    let $reload_button = $(`<button id="reload_button" type="button" title="Nach Netzwerken suchen." class="btn btn-primary">${icons.RELOAD}</button>`)
    let $disconnect_button = $(`<button id="disconnect_button" type="button" title="Verbindung trennen und mit Hotspot verbinden." class="btn btn-dark" disabled>${icons.NOWIFI}</button>`)
    let $add_button = $(`<button id="wifi_add_button" type="button" title="Verbindung manuell eingeben." class="btn btn-primary">${icons.PLUS}</button>`)

    $reload_button.click(e => {
      this.listReload()
    })
    $disconnect_button.click(e => {
      console.log("Disconnect and back to hotspot")
      this.disconnect()
    })
    $add_button.click(e => {
      console.log("Add WiFi manually.")
      this.options({ HIDDEN: true })
    })

    this.$modal.find(".modal-footer").children().remove()
    this.$modal.find(".modal-footer").append($disconnect_button).append($add_button).append($reload_button)

    socket.off('wifi_list')
    socket.on('wifi_list', this.listUpdate.bind(this))
    this.listReload()
  }

  listReload() {
    this.loading("Suche Netzwerke ...")
    this.$modal.find('#reload_button').attr('disabled',true)
    socket.emit('wifi', {list:true})
  }

  listUpdate(wifilist) {
    console.log("WiFi list updated")
    let $list = $('<div class="list-group"></div>')
    let connected = false
    for(let wifi of wifilist) {
      let $net = $('<button type="button" class="list-group-item list-group-item-action">' + wifi.SSID + '</button>')
      
      if(wifi.CONNECTED) {
        $net.append('<div class="options_icon mx-1">' + icons.WIFI + '</div>')
        $net.addClass('active disabled')
        connected = true
        this.$modal.find('#disconnect_button').attr('disabled',false)
      } else {
        $net.click(e => {
          console.log("connect to " + wifi.SSID + " " + wifi.WPA)
          this.options(wifi)
        })
      }
      
      if(wifi.WPA) {
        $net.append('<div class="options_icon mx-1">' + icons.LOCK + '</div>')
      }
      
      $list.append($net)
    }

    if(wifilist.length) {
      this.$modal.find('.modal-body').html($list)
    } else {
      this.$modal.find('.modal-body').html('<div>Es konnte leider kein W-LAN in der Nähe gefunden werden.</div>')
    }
    
    if(!connected) {
      this.$modal.find('#disconnect_button').attr('disabled',true)
    }

    this.$modal.find('#reload_button').attr('disabled',false)
  }

  disconnect() {
    socket.off('wifi_list')
    this.$modal.find(".modal-body").html(`
      <div>
        <h4>Verbindung zurücksetzen</h4>
        <p>
          Beendet die aktuelle W-LAN Verbindung und startet den <b>audioding</b> Hotspot. Die Internetverbindung wird damit getrennt.
        </p>
      </div>
    `)

    let $hotspot_button = $(`<button id="wifi_hotspot_button" class="btn btn-warning" type="button">Trennen</button>`)

    $hotspot_button.click(e => {
      console.log("disconnect wifi. Restart Hotspot")
      socket.emit('wifi', {disconnect:true})

      socket.on( 'connect', () => {
        this.close()
      })

      this.$modal.find(".modal-body").html(`
        <div>
          <h4>Verbindung wird getrennt</h4>
          <p>
            Um wieder auf diese Seite zuzugreifen, verbinde dich mit dem <b>audioding</b> netzwerk.
          </p>
        </div>
        <br>
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" role="status">
            <span class="sr-only"></span>
          </div>
        </div>
      `)

      this.$modal.find(".modal-footer").children("#wifi_hotspot_button").attr('disabled',true)
    })

    let $back_button = $(`<button id="wifi_back_button" type="button" class="btn btn-primary">Zurück</button>`)

    $back_button.click(e => {
      this.edit()
    })
    
    this.$modal.find(".modal-footer").children().remove()
    this.$modal.find(".modal-footer").append($back_button).append($hotspot_button)
  }

  options(wifi) {
    socket.off('wifi_list')
    this.$modal.find(".modal-body").html(`
      <div>
        <h4>Mit <b>${wifi.SSID}</b> verbinden</h4>
      </div>
    `)

    if(wifi.HIDDEN) {
      this.$modal.find(".modal-body").html(`
      <div>
        <h4>Verbindung herstellen</h4>
        <p>
          Gib den Namen und ggf. das Passwort für dein W-LAN an um eine Internetverbindung herzustellen.
        </p>
      </div>
    `)
      this.$modal.find(".modal-body").append(`
        <form id="wpa_ssid_form">
          <div class="form-group">
            <label for="wpa_ssid_input" class="font-weight-bold">Netzwerk Name (SSID)</label>
            <input type="text" class="form-control" id="wpa_ssid_input" pattern=".{1,32}" required>
          </div>
        </form>
        <form id="wpa_psktype_form">
          <div class="form-group">
            <label for="wpa_psktype_select" class="font-weight-bold">Sicherheit</label>
            <select class="custom-select" id="wpa_psktype_select" required>
              <option value="NONE" selected>Keine</option>
              <option value="WPA">WPA/WPA2 PSK</option>
            </select>
          </div>
        </form>
      `)
    }

    this.$modal.find(".modal-body").append(`
      <div id="wifi_passwort_settings">
        <form id="wpa_password_form">
          <div class="form-group">
            <label for="wpa_password_input" class="font-weight-bold">Passwort</label>
            <input type="password" class="form-control" id="wpa_password_input" pattern=".{8,63}" required>
          </div>
        </form>
        <div class="form-group form-check">
          <input type="checkbox" class="form-check-input" id="show_hide_password">
          <label class="form-check-label" for="show_hide_password">Passwort anzeigen</label>
        </div>
      </div>
    `)

    if(!wifi.WPA) {
      $("#wifi_passwort_settings").hide()
    }

    let $connect_button = $(`<button id="wifi_connect_button" class="btn btn-success" type="button">Verbinden</button>`)

    $connect_button.click(e => {
      if(wifi.HIDDEN) {
        //$("#wpa_ssid_input").removeClass("was-validated")
        wifi.SSID = $("#wpa_ssid_input").val()
      }

      console.log("connect wifi")
      console.log(wifi.SSID)

      if(wifi.SSID.length > 0 && wifi.SSID.length <= 32) {
        //$("#wpa_ssid_input").addClass("is-valid")
        //$("#wpa_password_form").addClass("was-validated")
        if(wifi.WPA) {
          console.log($("#wpa_password_input").val())
          $("#wpa_password_input").addClass("is-invalid")
          $("#wpa_password_form").addClass("was-validated")
          if($("#wpa_password_input").val().length >= 8 && $("#wpa_password_input").val().length <= 63) {
            socket.emit('wifi', {connect:{SID:wifi.SSID, PSK:$("#wpa_password_input").val()}})
            this.connect(wifi)
          } else {
            console.error("Can't connect to " + wifi.SSID + ". Password field not filled out.")
          }
        } else {
          socket.emit('wifi', {connect:{SID:wifi.SSID, PSK:undefined}})
          this.connect(wifi)
        }
      } else {
        console.error("Can't connect. Network SSID missing.")
        $("#wpa_ssid_input").addClass("is-invalid")
        $("#wpa_ssid_form").addClass("was-validated")
      }
      
    })

    $("#show_hide_password").click(e => {
      if($("#show_hide_password").is(':checked')) {
        $("#wpa_password_input").attr('type', 'text')
      } else {
        $("#wpa_password_input").attr('type', 'password')
      }
    })

    $('#wpa_psktype_select').click(e => {
      if($('#wpa_psktype_select option:selected').val() == "NONE") {
        console.log("Hide password option")
        $("#wifi_passwort_settings").hide()
        wifi.WPA = false
      } else if($('#wpa_psktype_select option:selected').val() == "WPA") {
        console.log("show password option")
        $("#wifi_passwort_settings").show()
        wifi.WPA = true
      }
    })

    let $back_button = $(`<button id="wifi_back_button" type="button" class="btn btn-primary">Zurück</button>`)

    $back_button.click(e => {
      this.edit()
    })
    

    this.$modal.find(".modal-footer").children().remove()
    this.$modal.find(".modal-footer").append($back_button).append($connect_button)
  }

  connect(wifi) {
    this.$modal.find(".modal-body").html(`
        <div>
          <h4>${wifi.SSID} wird verbunden</h4>
          <p>
            Verbinde dich ebenfalls mit <b>${wifi.SSID}</b> um wieder auf diese Seite zuzugreifen.
          </p>
          <p>
            Sollte die Verbindung zu <b>${wifi.SSID}</b> fehlschlagen, wird der audioding Hotspot nach kurzer Zeit wieder eingerichtet.
          </p>
        </div>
        <br>
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" role="status">
            <span class="sr-only"></span>
          </div>
        </div>
      `)

      socket.on( 'connect', () => {
        this.close()
      })

      this.$modal.find(".modal-footer").children("#wifi_connect_button").attr('disabled',true)
  }
}

class SpotifySettings extends Settings {
  constructor() {
    super("spotify", "Spotify", icons.SPOTIFY)
    this.$button.prop('title','Spotify Anmeldung und Einstellungen')
  }

  edit() {
    socket.emit('spotify', {"auth_status":""}, response => {
      if(response.error) {
        console.error(response.error.message)
        switch(response.error.type) {
          case "ConnectError":
            this.noWifi()
            break
          case "SpotifyAuthError":
          case "SpotifyException":
            this.loginFailed(response.error.message)
            break
        }
      }
      else if(response["authenticated"]) {
        if(!response["device"]) {
          this.connectInstructions(response["user"])
        } else if(response["device"].is_local_device) {
          this.loggedIn(response["user"])
        } else {
          this.connectInstructions(response["user"], response["device"])
        }
      } else {
        this.loggedOff()
      }
    })
  }

  connectInstructions(user, device) {
    this.$modal.find(".modal-body").html(`<p>Du bist eingeloggt als <a href="${user["external_urls"]["spotify"]}" target="_blank" class="text-center"><strong>${user["display_name"]}</strong></a>.</p>`)
    
    if(device) {
      this.$modal.find(".modal-body").append(`<p>Spotify Audio wird derzeit über <strong>${device.name} (${device.type})</strong> abgespielt.</p>`)
    }

    this.$modal.find(".modal-body").append(`<p>Verbinde den audioding speaker über die Spotify App oder Desktop Anwendung um ihn als Ausgabegerät zu nutzen.</p>`)
    this.$modal.find(".modal-body").append(`<img src="../static/img/connect_device.png" class="rounded mx-auto d-block img-fluid" alt="Klicke auf 'Gerät verbinden'.">`)
    this.$modal.find(".modal-body").append(`<br><p>Eine detaillierte Anleitung findest du auf der <a href="https://www.spotify.com/de/connect/" target="_blank">Spotify Connect</a> Webseite.</p>`)
    
    /*
    this.$modal.find(".modal-body").append(`
      <div class="row">
        <div class="col"><img src="static/img/connect_101.png" class="rounded img-fluid" alt="Klicke im Abspielmodus auf das eräte Icon."></div>
        <div class="col"><img src="static/img/connect_102.png" class="rounded img-fluid" alt="Wähle den Audioding Speaker aus."></div>
      </div>
    `)
    */

    this.$modal.find(".modal-footer").children().remove()
    let $reload_button = $(`<button id="reload_button" type="button" title="Neu laden." class="btn btn-primary">${icons.RELOAD}</button>`)
    $reload_button.click(e => {
      this.loading()
      this.edit()
    })
    let $logoff_button = this.logOff()
    this.$modal.find(".modal-footer").append($logoff_button)
    this.$modal.find(".modal-footer").append($reload_button)
  }

  /**
   * This function is deprecated since it is more convinient to use device auth through an official app.
   * 
   * @param {*} user 
   * @param {*} device 
   */
  connectDevice(user, device) {
    this.$modal.find(".modal-body").html(`<p>Du bist eingeloggt als</p><a href="${user["external_urls"]["spotify"]}" target="_blank" class="text-center"><p><strong>${user["display_name"]}</strong></p></a>`)
    let $connect_button = $(`<button id="spotify_device_connect_button" type="button" title="Spotify Streaming Player Verbinden." class="btn btn-primary">Gerät Verbinden</button>`)

    if(device) {
      this.$modal.find(".modal-body").append(`<p>Spotify Audio wird derzeit über <strong>${device.name} (${device.type})</strong> abgespielt.</p>`)
    }
    this.$modal.find(".modal-body").append(`<p>Gib dein Passwort an um das audioding als Audio Ausgabe Gerät verwenden zu können.</p>`)
    // Info proposal: `<p>Dieses Audioding ist noch nicht als streaming player verbunden und kann noch kein Spotify Audio wiedergeben. Du kannst die Spotify app nutzen und das audioding als Wiedergabe Gerät auswählen. Um das audioding über einen Neustart hinaus als Wiedergabegerät zu nutzen gib dein Spotify login password hier an.</p>`
    $connect_button.click(e => {
      let passwd = this.validatePassword('spotify_device',1,9999)
      if(passwd) {
        this.loading("Spotify Audioplayer wird eingerichtet ...")
        this.$modal.find(".modal-footer").children().remove()
        socket.emit('spotify', {"device_login":{"password":passwd,"username":user.display_name}}, response => {
          console.log(response)
          if(!response) {
            this.connectDevice(user)
            main.error('Spotify Fehler', 'Audioding Streaming player konnte nicht eingerichtet werden')
          } else if(response.is_local_device) {
            main.info('Spotify','Audioding Gerät erfolgreich verbunden')
            this.loggedIn(user)
          } else {
            this.connectDevice(user, response)
            main.warn('Spotify Warnung',`Audioding Gerät konnte nicht verbunden werden. Audio Ausgabe stattdessen auf ${response.name}.`)
          }
        })
      }
    })

    this.appendPasswordForm('spotify_device', 1, 9999)

    let $logoff_button = this.logOff()

    this.$modal.find(".modal-footer").children().remove()
    
    this.$modal.find(".modal-footer").append($logoff_button)
    this.$modal.find(".modal-footer").append($connect_button)
  }

  loggedIn(user) {
    let $logoff_button = this.logOff()

    this.$modal.find(".modal-body").html(`<h1 class="display-4 text-center">${icons.SPEAKER}<span class="text-success">${icons.CHECK}</span>${icons.SPOTIFY}</h1>`)
    this.$modal.find(".modal-body").append(`<br><p>Du bist eingeloggt als <a href="${user["external_urls"]["spotify"]}" target="_blank" class="text-center"><strong>${user["display_name"]}</strong>.</p></a><p>Lege ein Ding auf, um es mit einer Spotify Playlist zu verknüpfen.</p>`)

    this.$modal.find(".modal-footer").children().remove()
    this.$modal.find(".modal-footer").append($logoff_button)
  }

  logOff() {
    let $logoff_button = $(`<button id="logoff_button" type="button" title="Anderen Benutzer anmelden." class="btn btn-primary">${icons.USER}Nutzer wechseln</button>`)

    $logoff_button.click(e => {
      socket.emit('spotify', {"change_login":"audioding_user"}, response => {
        if (response["url"]) {
          window.open(response["url"], "_self")
        }
      })
    })

    return $logoff_button
  }

  loggedOff() {
    main.error('Spotify Verbindungsfehler', 'Du bist derzeit nicht bei Spotify eingeloggt.')
    
    let $login_button = $(`<button id="login_button" type="button" title="Bei Spotify einloggen." class="btn btn-primary">Einloggen</button>`)
    
    $login_button.click(e => {
      socket.emit('spotify', {"login":"audioding_user"}, response => {
        if (response["url"]) {
          window.open(response["url"], "_self")
        }
      })
    })

    this.$modal.find(".modal-body").html(`<p>Du bist nicht eingeloggt.</p><p>Logge dich in deinem Spotify Account ein, um deine Playlists mit einem Ding zu verknüpfen.</p>`)

    this.$modal.find(".modal-footer").children().remove()
    this.$modal.find(".modal-footer").append($login_button)
  }

  noWifi() {
    this.$modal.find(".modal-body").html(`<p>Internet Verbindung konnte nicht hergestellt werden.</p>
                                          <p class="text-center">${icons.NOWIFI}</p>
                                          <p>Öffne die <a href="${window.location.origin}/settings/wifi">W-Lan Einstellungen</a> um das Audioding mit deinem Heimnetzwerk zu verbinden.</p>`)

    this.$modal.find(".modal-footer").children().remove()
  }

  loginFailed(error) {
    this.$modal.find(".modal-body").html(`<p class="text-center text-danger">${icons.DANGER}</p><p>Login fehlgeschlagen. Der Spotify Server hat folgende Fehlermeldung angegeben:</p><p class="text-danger">"${error}"</p>`)
    
    this.$modal.find(".modal-footer").children().remove()
    let $logoff_button = this.logOff()
    this.$modal.find(".modal-footer").append($logoff_button)
  }
}

class AudioSettings extends Settings {
  constructor() {
    super('audio', 'Audio', icons.SPEAKER)
    this.$button.prop('title','Audio Einstellungen')
  }

  edit(restartInfo) {
    socket.emit("get_settings", {}, settings => {
      this.reload(settings, restartInfo)
    })
  }

  reload(settings, restartInfo) {
    this.$modal.find('.modal-body').html(`<p>Aktiviere Stereo Sound wenn du einen zweiten Lautsprecher angeschlossen hast.</p>`)

    let $speaker_toggle = $(`
      <div class="m-2">
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="toggleMono" name="speakerSetupToggle" class="custom-control-input" value="mono">
          <label class="custom-control-label" for="toggleMono">Mono</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="toggleStereo" name="speakerSetupToggle" class="custom-control-input" value="stereo">
          <label class="custom-control-label" for="toggleStereo">Stereo</label>
        </div>
      </div>
    `)

    if(settings.channels == 1) {
      $speaker_toggle.find('#toggleMono').prop({'checked': true})
      $speaker_toggle.find('#toggleStereo').removeProp('checked')
      console.log("Speaker setup is Mono")
    } else if(settings.channels == 2) {
      $speaker_toggle.find('#toggleStereo').prop({'checked': true})
      $speaker_toggle.find('#toggleMono').removeProp('checked')
      console.log("Speaker setup is Stereo")
    }

    this.$modal.find('.modal-body').append($speaker_toggle)

    if(restartInfo) {
      this.hint("info", `Um die Änderungen für die Wiedergabe von Audiodateien anzuwenden musst du das audioding <a href="${window.location.origin}/settings/power">neu starten</a>.`)
    }

    $speaker_toggle.find('input[type=radio][name=speakerSetupToggle]').change(e => {
      console.log(e.target.value)
      if (e.target.value == 'mono') {
          this.loading()
          socket.emit('speaker_channels', {channels:1}, result => {
            main.info("Audio Settings", "Mono Lautsprecher aktiviert.")
            this.edit(true)
          })
      } else if (e.target.value == 'stereo') {
        this.loading()
        socket.emit('speaker_channels', {channels:2}, result => {
          main.info("Audio Settings", "Stereo Lautsprecher aktiviert.")
          this.edit(true)
        })
      }
    })
  }
}