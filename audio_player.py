"""use pygame music to control audio playback
see https://www.pygame.org/docs/ref/music.html
uses mutagen module to obtain file info of audio files

todo: use ding setup function on first assignment
"""
import pygame
import threading
import mutagen
import time

ding = None
thread_active = False
in_pause = False

callback = None
timestamp = 0 # takes a play position snapshot whenever music play position is changed (on set_pos and start)

def init(event_callback=None):
    """
    initialize audio player and pygame mixer

    Quit mixer if it was already initialized

    Args:
        event_callback (Function): main callback function
    """
    if event_callback is not None:
        global callback
        callback = event_callback
    
    if pygame.mixer.get_init() is not None:
        pygame.mixer.quit()

    pygame.mixer.init()

def load(ding):
    pass

def command(msg):
    """control pygame audio player using app callback

    Args:
        msg (Dict): specify audio player command
    """
    if 'switch_track' in msg:
        switch(msg['switch_track'])
    elif 'set_volume' in msg:
        set_volume(msg['set_volume'])
    elif 'volume_up' in msg:
        volume_up(msg['volume_up'])
    elif 'volume_down' in msg:
        volume_down(msg['volume_down'])
    elif 'playback' in msg:
        if msg['playback'] == 'pause':
            pause()
        elif msg['playback'] == 'resume':
            resume()
        elif msg['playback'] == 'stop':
            stop()
        elif msg['playback'] == 'skip_start':
            skip_start()
        elif msg['playback'] == 'skip_end':
            skip_end()
    elif 'position' in msg:
        set_position(msg['position'])
    elif 'backward' in msg:
        backward(msg['backward'])
    elif 'forward' in msg:
        forward(msg['forward'])
    else:
        print("Uknown audio player command %s" % str(msg))
    
def skip_end():
    if not ding:
        print("can not set skip end. No Ding present.")
        return
    set_position(ding.current_track["duration"])
    print("skip to end of track")
    
def skip_start():
    if not ding:
        print("can not set skip to start. No Ding present.")
        return
    set_position(0)
    print("skip to beginning of track")
    
def forward(seconds):
    if not ding:
        print("can not wind forwards. No Ding present.")
        return
    set_position(get_position() + seconds)
    print("wind forwards %g seconds " % seconds)
    
def backward(seconds):
    if not ding:
        print("can not wind backwards. No Ding present.")
        return
    set_position(get_position() - seconds)
    print("wind backwards %g seconds " % seconds)
    
def switch(track_id):
    if ding is not None:
        track = ding.getTrack('id', track_id)
        print("change track to %s" % track['filename'])
        ding.set('current_track', track)
        start_play(ding, 0)
    else:
        print("could not switch to track %s. No Ding found for audio player." % track_id)
    
def replay():
    print("play from start")

def pause():
    global in_pause
    print('pause playback')
    in_pause = True
    send_status()
    pygame.mixer.music.pause()
    
def resume():
    global in_pause
    print('resume playback')
    in_pause = False
    send_status()
    pygame.mixer.music.unpause()

#
# the ding 'play_position' property needs to be updated whenever there is a change made to the running playback
def set_position(pos):
    if not ding:
        print("can not set position. No Ding present.")
        return
    
    if pos < 0:
        pos = 0
        
    if pos < ding.current_track['duration']-1.0:
        the_pos = pygame.mixer.music.get_pos()/1000
        print('mixer.music play position is %g' % the_pos)
        pygame.mixer.music.rewind()
        pygame.mixer.music.set_pos(pos)
        store_position(pos)
    else:
        print('set playposition to track end (%g). Will stop track.' % pos)
        pygame.mixer.music.stop()

def store_position(pos=None):
    global timestamp
    music_get_pos = pygame.mixer.music.get_pos()/1000
    if pos is not None:
        ding.set('play_position', pos)
        print('store play position %g, timestamp %g, music.get_pos %g' % (pos, timestamp, music_get_pos))
        timestamp = music_get_pos
        print('position is now %g' % get_position())
    else:
        ding.set('play_position', get_position())
        print('store current play position. timestamp %g, music.get_pos %g' % (timestamp, music_get_pos))
        timestamp = music_get_pos
        print('position is now %g' % get_position())
    send_status()

#
# nomatter what is done with the running audio (rewind, pause, set_pos)
# music.get_pos only returns how much time passed since playback started
#
def get_position():
    global timestamp
    played_total = pygame.mixer.music.get_pos()/1000
    since_last_set = played_total - timestamp
    position = ding.play_position + since_last_set
    return position

def set_volume(val):
    if val >= 1:
        val = 1
    elif val <= 0:
        val = 0

    if ding:
        ding.set('volume', val)
    pygame.mixer.music.set_volume(val)
    send_status()
    
def get_volume():
    return pygame.mixer.music.get_volume()

def volume_up(val):
    set_volume(get_volume() + val)

def volume_down(val):
    set_volume(get_volume() - val)

def reorder(options):
    pass

def assign(ding, content, volume):
    # TODO this is never called! But it would make sense to use it. The way it is is ver confusing and happens within the audiofile upload process
    ding.setup("files",{'volume':volume,'options':['add','remove','clear']})

def play(item):
    global thread_active
    print('start audio player thread')
    set_volume(item.volume)
    thread_active = True
    audio_thread = threading.Thread(target = play_audio, name = 'audio_player', args = (item,))
    audio_thread.start()

def play_audio(item):
    global ding
    
    next = False
    ding = item
    
    if not ding.current_track:
        ding.set('current_track', ding.tracks[0])
    
    start_play(ding, ding.play_position)
    
    while thread_active:
        #if pygame.mixer.music.get_pos() < 0:
        if not pygame.mixer.music.get_busy() and not in_pause:
            if not next:
                print("track ended")
                next = True
                
                # if ding was removed
                if not ding.current_track:
                    print("current track was removed. Stop playback")
                    send_status()
                else:
                    current_index = ding.getTrackIndex('id',ding.current_track['id'])
                    print("Current Track Index %d" % current_index)
                
                    if len(ding.tracks) > current_index + 1:
                        ding.set("current_track", ding.tracks[current_index + 1])
                        start_play(ding, 0)
                        #pygame.mixer.music.load("audio/%s/%s" % (ding.id, ding.current_track))
                        #pygame.mixer.music.play(loops=0, start=0)
                        #print("start next track %s." % ding.current_track)
                        ##pygame.mixer.music.queue("audio/%s" % ding.tracks[ding.current_track+1])
                    else:
                        print("no more tracks in playlist. Stop playback")
                        ding.set("current_track", {})
                        ding.set("play_position", 0.0)
                        send_status()
        else:
            time.sleep(.1)
            next = False
    
            
def start_play(ding, start_pos):
    global in_pause
    global timestamp
    timestamp = 0
    in_pause = False
    ding.set('play_position', start_pos)
    file_info = mutagen.File("audio/%s/%s" % (ding.id, ding.current_track["filename"]))
    ding.set('current_track.duration', file_info.info.length)
    try:
        pygame.mixer.music.load("audio/%s/%s" % (ding.id, ding.current_track["filename"]))
        #pygame.mixer.music.set_pos(0)
        pygame.mixer.music.play(loops=0, start=start_pos)
        print("start %s at %g sec. track length %g" %(ding.current_track["filename"], ding.play_position, ding.current_track['duration']))
        send_status()
    except pygame.error as e:
        callback("alert",{"type":"danger","title":"Audio Player Fehler","message":"%s in Track: %s" % (str(e), ding.current_track["filename"])})
    
def send_status():
    callback("play_status",get_status())
        
def get_status():
    global in_pause
    
    if ding:
        return {"track":ding.current_track, "position":get_position(), "pause":in_pause, "volume":get_volume()}
    else:
        return {"position":0.0, "duration":0.0, "pause":False, "volume":get_volume()}

def tone(file):
    """just play a file without storing progress to ding

    Args:
        file (str): filename
    """
    pygame.mixer.music.load(file)
    pygame.mixer.music.play(0)

def mixer_status():
    return {"busy":pygame.mixer.music.get_busy(), "position":get_position(), "timestamp":timestamp, "mixer_position":pygame.mixer.music.get_pos()/1000}

def stop():
    global ding
    global thread_active
    
    if ding is not None:
        played_total = pygame.mixer.music.get_pos()/1000
        print('stop at get_pos %g' % played_total)
        #if played_total < 0.0:
        if not pygame.mixer.music.get_busy():
            ding.set("play_position", 0)
            print("track has ended. reset playposition to 0.0 sec.")
        else:
            # new_playposition = played_total + ding.play_position
            # print("stop track at %g sec." % new_playposition)
            # ding.set("play_position", new_playposition)
            store_position()
    
    pygame.mixer.music.stop()
    ding = None
    thread_active = False

class Play(object):
    def __init__(self, ding):
        self.ding = ding
