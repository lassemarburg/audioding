"""host server, audio player and other modules

for cross module interaction :func:`event_callback` is distributing/forwarding commands 
"""
import signal, threading, sys, time, os, subprocess, json, time, shutil
import server, audio_player, collection, wifi, spotify_api

try:
    import reader
    use_reader = True
except ImportError:
    use_reader = False
    print("WARNING: Failed to load NFC Reader. NFC Functions disabled.")
    pass

try:
    import control
    use_control = True
except ImportError:
    use_control = False
    print("WARNING: Failed to load GPIO Control. Control Functions disabled.")
    pass
    
# global function to register a quit operation
def signal_handler(signal, frame):
    quit()
    print("Close Application")
    
    sys.exit(0)
    
signal.signal(signal.SIGINT, signal_handler)

active = True

dinge = None
spotify = None
config = None

def quit():
    """stop all modules
    """

    global active
    active = False

    get_current_player().stop()
    if use_reader:
        reader.end()
    if use_control:
        control.quit()

    server.quit()

def ready():
    """once everything is up and loaded
    """
    audio_player.tone('jingle.mp3')
    
def event_callback(cmd, msg=None):
    if cmd == 'client_connected':
        client_connected()
    elif cmd == 'established':
        ding_established(msg)
    elif cmd == 'assign':
        ding_assignment(msg)
    elif cmd == 'alert':
        server.alert(msg)
        if msg['type'] == 'danger':
            audio_player.tone('error.mp3')
    elif cmd == 'get_settings':
        return load_settings()
    elif cmd == 'speaker_channels':
        return speaker_channels(msg["channels"])
    elif cmd == 'files_added':
        print('successfully added files. Update Webview.')
        server.show(msg.get_dict())
        server.play_status(audio_player.get_status())
    elif cmd == 'files_removed':
        server.show(msg.get_dict())
    elif cmd == 'reload':
        ding_established(dinge.present)
    elif cmd == 'clear':
        get_current_player().stop()
        #time.sleep(.1)
        dinge.present.setup('empty', {})
        # server.show(dinge.present.get_dict())
        server.new(dinge.present.id)
    elif cmd == 'play_status':
        server.play_status(msg)
    elif cmd == 'audio':
        get_current_player().command(msg)
        if 'set_volume' in msg and dinge.present is None:
            change_settings("idle_volume", msg['set_volume'])
    elif cmd == 'reorder':
        return get_current_player().reorder(msg)
    elif cmd == 'ding_detected':
        ding_there(msg)
    elif cmd == 'ding_removed':
        ding_gone()
    elif cmd == 'reboot':
        return shutdown('reboot')
    elif cmd == 'shutdown':
        #shutdown('halt')
        return shutdown('poweroff')
    elif cmd == 'wifi':
        if 'list' in msg:
            server.wifi_list(wifi.scan())
        elif 'connect' in msg:
            if wifi.connect(msg["connect"]):
                spotify.raspotify('restart')
                set_wifi_led(True)
            else:
                set_wifi_led(False)
        elif 'disconnect' in msg:
            if wifi.disconnect():
                set_wifi_led(False)
            else:
                set_wifi_led(True)
        elif 'get_status' in msg:
            wifi_status = wifi.get_status()
            server.wifi_status(wifi_status)
            set_wifi_led(wifi_status)
        #elif 'get_netowrks' in msg:
        #    server.networks(wifi.get_networks()) # dummy for implementing to save networks in a seperate json
    elif cmd == 'spotify':
        return spotify.command(msg)
    else:
        raise Exception("Unknown command %s" % cmd) 

def client_connected():
    if dinge.present is not None:
        if not dinge.present_is_new():
            server.show(dinge.present.get_dict())
            server.play_status(get_current_player().get_status())
        else:
            server.new(dinge.present.id)
    else:
        server.empty(audio_player.get_status())

def set_wifi_led(online):
    if use_control:
        if online:
            control.set_network_led("on")
        else:
            control.set_network_led("off")

def get_player(type):
    if type == "files":
        return audio_player
    elif type == "spotify":
        return spotify
    elif type == "empty":
        return audio_player

def get_current_player():
    if dinge.present is not None:
        return get_player(dinge.present.type)
    
    return audio_player

# reader callback when a tag is placed onto the antenna
def ding_there(id):
    """tag was placed on the antenna

    set this tag to be the present ding. If it is new, allow assignment first, otherwise directly show playlist and play current track.

    Args:
        id (str): tag id
    """
    dinge.set_present(id)
    if not dinge.present_is_new():
        audio_player.tone('there.mp3')
        ding_established(dinge.present)
    else:
        audio_player.tone('new.mp3')
        server.new(id)

# reader callback when the tag is removed from antenna
def ding_gone():
    server.empty(get_current_player().get_status())
    get_current_player().stop()
    settings = load_settings()
    audio_player.set_volume(settings["idle_volume"])
    
    dinge.remove_present()

def ding_assignment(content):
    """assign a new playlist to a new or existing ding

    previous playlist data is removed.

    Args:
        content (dict): ding id and attributes to be assigned to ding
    """
    ding = dinge.find('id',content['id'])
    if not ding:
        ding = dinge.add({'id':content['id']})
    else:
        ding.clear()

    settings = load_settings()

    get_player(content['type']).assign(ding, content, settings["idle_volume"])

    ding_established(ding)
    
def ding_established(ding):
    """show ding content on webview and play audio

    Args:
        ding (collection.Ding): the ding that got new content assigned
    """
    get_player(ding.type).load(ding)

    server.show(ding.get_dict())

    get_player(ding.type).play(ding)

# Command line input
def checkinput():
    newline = input("\n")
    in_list = newline.split(" ")
    
    if len(in_list) == 1:
        if in_list[0] == "remove":
            ding_gone()
            return
        if in_list[0] == "mixer":
            print(audio_player.mixer_status())
            return
        if in_list[0] == "volume_up":
            get_current_player().volume_up(0.1)
            return
        if in_list[0] == "volume_down":
            get_current_player().volume_down(0.1)
            return
        if in_list[0] == "play":
            get_current_player().command({"playback":"play"})
            return
        if in_list[0] == "pause":
            get_current_player().command({"playback":"pause"})
            return
        if in_list[0] == "devices":
            dev = spotify.get_device()
            print(dev)
            return
    elif len(in_list) == 2:
        # tag there simulation
        if in_list[0] == "place":
            ding_there(in_list[1])
            return
    elif len(in_list) == 3:
        if in_list[0] == "find":
            dinge.find(in_list[1], in_list[2])
            return
    
    print('invalid command %s' % str(in_list))
    
def shutdown(cmd):
    """shutdown or reboot raspberry

    cmd poweroff will call custom shutdown file to allow cutting the power with onoff shim

    Args:
        cmd (string): maybe 'reboot', 'halt' or 'poweroff'
    """
    command = ''
    if cmd == 'halt':
        command = "/usr/bin/sudo /sbin/shutdown -h now"
    if cmd == 'reboot':
        command = "/usr/bin/sudo /sbin/shutdown -r now"
    if cmd == 'poweroff':
        command = "poweroff"
    print('system command %s' % command)

    if use_control:
        control.set_status_led("blink")

    try:
        process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
        output = process.communicate()[0]
        print(output)
        return {"info":output}
    except FileNotFoundError as e:
        print(e)
        return {"error":{"message":str(e),"type":"FileNotFoundError"}}

def load_settings():
    """
    open settings.json and parse content to dict

    if settings.json does not exist create with default settings

    Returns:
            obj: settings dictionary
    """
    if not os.path.isfile("settings.json"):
        with open("settings.json", 'w') as outfile:
            default_settings = {
                "idle_volume": 0.2,
                "max_volume": 1.0,
                "channels":1
            }
            json.dump(default_settings, outfile, indent=2)
            return default_settings
            
    with open("settings.json") as json_file:
        return json.load(json_file)

def change_settings(key, value):
    """
    change a value in settings file

    Args:
        key (string): setting parameter to be saved
        value (string): change parameter to value
    """
    settings = load_settings()
    settings[key] = value
    with open("settings.json", 'w') as outfile:
            json.dump(settings, outfile, indent=2)

def speaker_channels(channels=1):
    alsa_config_file = "/etc/asound.conf"

    if config["environment"] == "test":
        alsa_config_file = "asound.conf"

    print("Change soud output for %s to %d channels." % (config["amp"], channels))

    if config["amp"] == "hifiberry":
        if channels == 1:
            shutil.copy("asound/hifiberry_mono.conf", alsa_config_file)
            change_settings('channels', 1)
        elif channels == 2:
            shutil.copy("asound/hifiberry_stereo.conf", alsa_config_file)
            change_settings('channels', 2)
    elif config["amp"] == "speaker_bonnet":
        if channels == 1:
            shutil.copy("asound/bonnet_mono.conf", alsa_config_file)
            change_settings('channels', 1)
        elif channels == 2:
            shutil.copy("asound/bonnet_stereo.conf", alsa_config_file)
            change_settings('channels', 2)
    
    if config["environment"] == "test":
        return
        
    spotify.raspotify("restart")
    
if __name__ == '__main__':
    with open("config.json") as config_file:
        config = json.load(config_file)

    argv = sys.argv[1:]
    
    port = 5000
    host = "localhost"

    if len(argv) > 0:
        port = int(argv[0])
    elif "port" in config:
        port = config["port"]

    if len(argv) > 1:
        host = argv[1]
    elif "hostname" in config:
        host = config["hostname"]
    
    if len(argv) > 2:
        wifi.interface(argv[2])
    elif "wifi_interface" in config:
        wifi.interface(config["wifi_interface"])
        
    if not os.path.isdir('./audio'):
        os.mkdir('./audio')
        print("create directory for audiofiles.")

    if port == 80:
        config["spotify_redirect_url"] = "http://%s/spotify_auth_code" % host
    else:
        config["spotify_redirect_url"] = "http://%s:%d/spotify_auth_code" % (host, port)
    
    print(config)

    settings = load_settings()

    audio_player.init(event_callback)
    audio_player.set_volume(settings["idle_volume"])

    spotify = spotify_api.SpotifyAPI(config, event_callback)
    spotifyRetry = 0
    spotifyConnected = False
    while spotifyRetry <= 10 and not spotifyConnected:
        spotifyConnected = spotify.init()
        if not spotifyConnected:
            spotifyRetry += 1
            print("Could not connect to spotify. Retry %i" % spotifyRetry)
            time.sleep(3)
        else:
            print("Spotify Connected")

    dinge = collection.Collection("store.json")
        
    server_thread = threading.Thread(target = server.run, name = 'webserver', args = (dinge,event_callback,port))
    server_thread.start()
    
    if use_reader:
        reader_thread = threading.Thread(target = reader.run, name = 'nfc_reader', args = (event_callback,))
        reader_thread.start()

    if use_control:
        control.init(event_callback)
    
    ready()

    print("Audioding internet connection:")
    wifi_status = wifi.get_status()
    set_wifi_led(wifi_status['online'])
    print(wifi_status)
    
    while active:
        checkinput()
        time.sleep(.100)
    
