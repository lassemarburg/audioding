import json, os, uuid

class Collection(object):
    def __init__(self, filename):
        self.filename = filename
        self.present = None
        self.dinge = []
        self.load()
    
    def load(self):
        if not os.path.isfile(self.filename):
            self.save()
            
        with open(self.filename) as json_file:
            data = json.load(json_file)
            for d in data["dinge"]:
                self.dinge.append(Ding(d, self.save))
        
    def save(self):
        #data = {"dinge":self.dinge}
        data = self.dict_coll()
        with open(self.filename, 'w') as outfile:
            json.dump(data, outfile, indent=2)
    
    def set_present(self, id):
        self.present = self.find('id', id)
        if not self.present:
            self.present = self.add({'id':id})
        
    def present_is_new(self):
        if self.present:
            if not self.present.empty():
                return False
        return True
        
    def remove_present(self):
        self.present = None
        
    def find(self, key, value):
        for d in self.dinge:
            #print(d.get(key), value)
            if d.get(key) == value:
                return d
        return False
        
    def add(self, document, volume=0.2):
        print("add new ding %s" % str(document))
        new_ding = Ding(document, self.save)
        new_ding.setup("empty", {'volume':volume})
        self.dinge.append(new_ding)
        self.save()
        return new_ding
        
    def remove(self, key, value):
        rem_ding = self.find(key, value)
        self.dinge.remove(rem_ding)
        self.save()
        
    def print_all(self):
        print("Dinge:")
        for ding in self.dinge:
            print(str(ding))
            
    def dict_coll(self):
        ret = {"dinge":[]}
        for ding in self.dinge:
            ret["dinge"].append(ding.get_dict())
        return ret
        

class Ding(object):
    """represents data assigned to 1 rfid tag

    all own properties are stored to collection.

    All functions that result in changes to properties end updating the collection file.

    Args:
        object (object): Based on python object
    """
    def __init__(self, document, save):
        self.update = save
        
        self._excluded_keys = set(self.__dict__.keys())
        
        for key in document:
            setattr(self, key, document[key])
        
        print("Create ding %s" % self.id)
    
    def setup(self, type, properties):
        """set new basic properties

        clear all previous properties if any

        Args:
            type (str): the base type (files or spotify)
            properties (dict): each dict property will be assigned to ding
        """
        self.clear()

        self.type = type

        # default audio playback properties
        self.tracks = []        # all tracks currently in playlist.
        self.volume = 0.5        # volume stored for this ding
        self.current_track = {} # track that is playing or paused
        self.play_position = 0. # current track play position in seconds
        self.track_length = 0.  # length of current track in seconds
        
        for attr in properties:
            setattr(self, attr, properties[attr])

        if 'name' not in properties:
            self.name = str(uuid.uuid4())

        print("Setup %s ding %s" % (self.type, self.id))

        self.update()

    def clear(self):
        """remove all own attributes except 'id'
        """ 
        if self.get('type') == "files":
            for track in self.tracks:
                os.remove("./audio/%s/%s" % (self.id, track['filename']))

        attributes = self.get_dict()
        for attr in attributes:
            if attr != 'id':
                delattr(self, attr)

        self.update()
        print("Ding %s is empty" % self.id)
        
    def empty(self):
        if hasattr(self, 'tracks'):
            if len(self.tracks):
                return False
        return True
    
    def get(self, property):
        return getattr(self, property, False)
        
    def set(self, key, value):
        setattr(self, key, value)
        self.update()

    def getTrack(self, key, value):
        for track in self.tracks:
            if track[key] == value:
                return track

    def getTrackIndex(self, key, value):
        for i, track in enumerate(self.tracks):
            if track[key] == value:
                return i

    def removeTrack(self, key, value):
        self.tracks[:] = [track for track in self.tracks if track.get(key) != value]
        
    def addAudiofile(self, filename, info):
        self.type = "files"
        self.options = ['add','remove','clear']
        for track in self.tracks:
            if filename == track["filename"]:
                print('%s is already in playlist of %s.' % (filename, self.id))
                return False

        self.tracks.append({"filename":filename,"name":filename,"duration":info.length,"id":str(uuid.uuid4())})
        self.update()
        return True
    
    def removeAudiofiles(self, filenames):
        ret_msg = ''
        for file in filenames:
            os.remove("./audio/%s/%s" % (self.id, file))
            self.removeTrack('filename', file)
            if self.current_track and self.current_track['filename'] == file:
                setattr(self,'current_track',{})
                setattr(self,'play_position',0.0)
                setattr(self,'track_length',0.0)
                ret_msg = 'stop_player'
            print('remove audio %s %s' % (self.id, file))
        self.update()
        return ret_msg
        
    def get_dict(self):
        #return self.__dict__
        return dict(
        (key, value)
        for (key, value) in self.__dict__.items()
            if key not in self._excluded_keys and key != "_excluded_keys"
        )
    
    def __repr__(self):
        #""""""
        return "%s" % self.get_dict()
    
