"""add playback, volume and power control options and status leds using gpio
"""

import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import time

app_callback = None

volume_up_pin = 18 # BCM 24
volume_down_pin = 13 # BCM 27

forward_pin = 36 # BCM 16 Conflict with HiFi Berry AMP! (https://www.hifiberry.com/docs/hardware/gpio-usage-of-hifiberry-boards/)
backward_pin = 33 # BCM 13

skip_start_pin = 15 # BCM 22
skip_end_pin = 16 # BCM 23
# onoffshim power off pin is 7 # BCM 4
power_pin = 11 # BCM 17

status_led_pin = 29 # BCM 5
status_led_blink_pin = 31 # BCM 6

network_led_pin = 32 # BCM 12

print("GPIO Setup")
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(volume_up_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(volume_down_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin to be an input pin and set initial v$
GPIO.setup(forward_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(backward_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(skip_start_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(skip_end_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(power_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

GPIO.setup(network_led_pin, GPIO.OUT)

#GPIO.setup(status_led_pin, GPIO.OUT)
#GPIO.setup(status_led_blink_pin, GPIO.OUT)

print("setup done")

active = True

def volume_down(channel):
    """gpio event callback to control volume down

    Args:
        channel (int): vol down button pin
    """
    print("leiser %d" % channel)
    app_callback("audio", {"volume_down":0.07})

def volume_up(channel):
    """gpio event callback to control volume up

    Args:
        channel (int): vol up button pin
    """
    print("lauter %d" % channel)
    app_callback("audio", {"volume_up":0.07})

def backward(channel):
    """gpio event callback to control playback backwards

    Args:
        channel (int): back button pin
    """
    print("zurück %d" % channel)
    app_callback("audio", {"backward":30})

def forward(channel):
    """gpio event callback to control playback forward

    Args:
        channel (int): forward button pin
    """
    print("vor %d" % channel)
    app_callback("audio", {"forward":30})

def skip_start(channel):
    print("skip start %d" % channel)
    app_callback("audio", {"playback":"skip_start"})

def skip_end(channel):
    print("skip end %d" % channel)
    app_callback("audio", {"playback":"skip_end"})

def set_network_led(mode):
    if mode == "off":
        GPIO.output(network_led_pin, GPIO.LOW)
    elif mode == "on":
        GPIO.output(network_led_pin, GPIO.HIGH)

def set_status_led(mode):
    if mode == "off":
        GPIO.setup(status_led_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(status_led_blink_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    elif mode == "on":
        GPIO.setup(status_led_blink_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(status_led_pin, GPIO.OUT)
        GPIO.output(status_led_pin, GPIO.HIGH)
    elif mode == "blink":
        GPIO.setup(status_led_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(status_led_blink_pin, GPIO.OUT)
        GPIO.output(status_led_blink_pin, GPIO.HIGH)

def power_down(channel):
    """gpio event callback to shutdown raspberry

    Args:
        channel (int): power button pin
    """
    timestamp = time.time()
    print("power button down %d at %d" % (channel, timestamp))
    duration = 0

    while GPIO.input(power_pin) == GPIO.LOW and duration <= 2:
        time.sleep(0.01)
        duration = time.time() - timestamp

        if duration > 2:
            print("poweroff")
            #GPIO.output(status_led_pin, GPIO.LOW)
            #time.sleep(0.2)
            #GPIO.output(status_led_pin, GPIO.HIGH)
            #time.sleep(0.2)
            #GPIO.output(status_led_pin, GPIO.LOW)
            #time.sleep(0.2)
            #GPIO.output(status_led_pin, GPIO.HIGH)
            app_callback("shutdown")

    print("power button up after %d sec." % duration)

def init(callback):
    """initiate gpio event handler

    Args:
        callback (function): app callback allowing access to other modules
    """    
    global app_callback
    app_callback = callback
    
    GPIO.add_event_detect(volume_down_pin, GPIO.RISING, callback=volume_down, bouncetime=300) # on release
    GPIO.add_event_detect(volume_up_pin, GPIO.RISING, callback=volume_up, bouncetime=300)
    GPIO.add_event_detect(backward_pin, GPIO.RISING, callback=backward, bouncetime=300)
    GPIO.add_event_detect(forward_pin, GPIO.RISING, callback=forward, bouncetime=300)
    GPIO.add_event_detect(skip_start_pin, GPIO.RISING, callback=skip_start, bouncetime=300)
    GPIO.add_event_detect(skip_end_pin, GPIO.RISING, callback=skip_end, bouncetime=300)

    GPIO.add_event_detect(power_pin, GPIO.FALLING, callback=power_down, bouncetime=500) # on press
    
    set_status_led("on")
    #GPIO.setup(status_led_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    #GPIO.output(status_led_pin, GPIO.LOW)
    #GPIO.output(status_led_blink_pin, GPIO.HIGH)

# tis seems to not happen when program quits due to poweroff
def quit():
    pass
    #GPIO.output(status_led_pin, GPIO.LOW)
    #GPIO.cleanup()
