#!/usr/bin/env python
# -*- coding: utf8 -*-
#
#    Copyright 2014,2018 Mario Gomez <mario.gomez@teubi.co>
#
#    This file is part of MFRC522-Python
#    MFRC522-Python is a simple Python implementation for
#    the MFRC522 NFC Card Reader for the Raspberry Pi.
#
#    MFRC522-Python is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    MFRC522-Python is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with MFRC522-Python.  If not, see <http://www.gnu.org/licenses/>.
#

import RPi.GPIO as GPIO
import MFRC522
import time

continue_reading = True
tag_present = False
remove_confirmed = False

# Capture SIGINT for cleanup when the script is aborted
def end():
    global continue_reading
    print("Reader stops")
    continue_reading = False
    GPIO.cleanup()

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

def run(callback):
    global tag_present
    global remove_confirmed
    
    # This loop keeps checking for chips. If one is near it will get the UID and authenticate
    while continue_reading:
        #print("ping")
        
        # Scan for cards    
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
        
        # If a card is found, reset remove confirmed var
        # else confirm and next time tell app once that card was removed
        if status == MIFAREReader.MI_OK:
            remove_confirmed = False
        elif tag_present:
            if remove_confirmed:
                tag_present = False
                print("Card removed")
                callback('ding_removed')
            remove_confirmed = True
        
        
        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        # tell the app once that card was detected and forward UID
        if status == MIFAREReader.MI_OK:
            if not tag_present:
                print("Card detected")
                print("Card read UID: %s,%s,%s,%s" % (uid[0], uid[1], uid[2], uid[3]))
                tag_present = True
                callback('ding_detected', ','.join([str(item) for item in uid ]))
            
                # This is the default key for authentication
                key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
                
                # Select the scanned tag
                MIFAREReader.MFRC522_SelectTag(uid)

                # Authenticate
                status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

                # Check if authenticated
                if status == MIFAREReader.MI_OK:
                    MIFAREReader.MFRC522_Read(8)
                    MIFAREReader.MFRC522_StopCrypto1()
                else:
                    print("Authentication error")
