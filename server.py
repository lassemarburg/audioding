from flask import Flask, render_template, request
from flask_socketio import SocketIO
from werkzeug.utils import secure_filename
import logging, os, mutagen

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'Y7*f.CCNQ8^t^k$+kzY&'
socketio = SocketIO(app, async_mode="threading") # not the optimal mode but there seems to be a problem with recomended eventlet https://flask-socketio.readthedocs.io/en/latest/api.html

terminate = False

dinge = None

connected = None

show_settings_on_connect = None

@app.route('/')
def index():
    if request.args.get("code"):
        callback('spotify', {"auth_code":request.args.get("code")})

    return render_template('index.html')

@app.route('/spotify_auth_code')
def spotif_auth_code():
    print("Request to /spotify_auth_code endpoint.")
    if request.args.get("code"):
        callback('spotify', {"auth_code":request.args.get("code")})
    global show_settings_on_connect
    show_settings_on_connect = "spotify"

    return render_template('index.html')

@app.route('/settings/<chapter>')
def settings(chapter):
    print("Request to /settings/%s endpoint." % chapter)
    global show_settings_on_connect
    show_settings_on_connect = chapter

    return render_template('index.html')

# get files from request and store them in audiofiles folder.
# append new audiofiles to playlist of ding
# 
@app.route('/upload', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        print('Upload files for ding %s' % request.form['id'])
        print(request.form)
        
        ding = dinge.find('id',request.form['id'])
        if not ding:
            settings = callback('get_settings')
            ding = dinge.add({'id':request.form['id']},settings['idle_volume'])
        
        for index, file in request.files.items():
            if not os.path.isdir('./audio/%s' % ding.id):
                os.mkdir('./audio/%s' % ding.id)
                print("create audio directory for %s." % ding.id)
            fp = os.path.join('./audio', ding.id, secure_filename(file.filename))
            file.save(fp)
            file_info = mutagen.File(fp)
            ding.addAudiofile(secure_filename(file.filename), file_info.info)
        
        if request.form['new'] == 'true':
            callback('established',ding)
        else:
            callback('files_added',ding)
            
        return {'response':'file uploaded successfully'}

@socketio.on('remove')
def remove(json, methods=['GET', 'POST']):
    ding = dinge.find('id',json['id'])
    if ding.removeAudiofiles(json["files"]) == 'stop_player':
        callback('audio',{'playback':'stop'})
    callback('files_removed', ding)

@socketio.on('load')
def on_load(json, methods=['GET', 'POST']):
    global show_settings_on_connect
    if show_settings_on_connect:
        show_settings(show_settings_on_connect)
        show_settings_on_connect = None
    
    callback('client_connected')

@socketio.on('reload')
def reload(json, methods=['GET', 'POST']):
    callback('reload')

@socketio.on('assign')
def assign(json, methods=['GET', 'POST']):
    """assign a ding to a playlist

    Args:
        json (Dict): information about the link/playlist and the ding id
        methods (list, optional): Socket request method. Defaults to ['GET', 'POST'].
    """
    callback('assign', json)

@socketio.on('audio')
def audio(json, methods=['GET', 'POST']):
    """Call audio command to control playback volume etc.
    
    Args:
        json (Dict): Audio command with additional parameters
        methods (list, optional): Socket request method. Defaults to ['GET', 'POST'].
    """
    print('audio %s' % str(json))
    callback('audio', json)
        
@socketio.on('log')
def on_log(json, methods=['GET', 'POST']):
    print(json)

@socketio.on('tracks')
def playlist(json, methods=['GET', 'POST']):
    callback('tracks', json)

@socketio.on('clear')
def clear(json, methods=['GET', 'POST']):
    callback('clear', json)
    
@socketio.on('reorder')
def reorder(json, methods=['GET', 'POST']):
    ding = dinge.find('id',json['id'])
    print('reorder tracks for %s' % json['id'])
    ding.set('tracks', json['tracks'])
    return callback('reorder',json)
    
@socketio.on('system')
def system(json, methods=['GET', 'POST']):
    print('system %s' % str(json))
    if 'reboot' in json:
        return callback('reboot')
    if 'shutdown' in json:
        return callback('shutdown')

@socketio.on('get_settings')
def get_settings(json, methods=['GET', 'POST']):
    return callback('get_settings', json)

@socketio.on('speaker_channels')
def speaker_channels(json, methods=['GET', 'POST']):
    return callback('speaker_channels', json)

@socketio.on('wifi')
def wifi(json, methods=['GET', 'POST']):
    print('wifi %s' % str(json))
    callback('wifi', json)

@socketio.on('spotify')
def spotify(json, methods=['GET', 'POST']):
    print('spotify %s' % str(json))
    return callback('spotify', json)

@socketio.on('stop')
def stop():
    print('Stop socketio server')
    socketio.stop()
    
def success(methods=['GET', 'POST']):
    print('socket emit successful')

def new(id):
    print("new ding: %s" % id)
    socketio.emit('new', {"id":id})

def show(document):
    socketio.emit('show', document)
    
def empty(status):
    socketio.emit('empty', status)
    
def play_status(status):
    socketio.emit('playing', status)
    print("emit play status %s" % str(status))

def wifi_list(wifilist):
    socketio.emit('wifi_list', wifilist)

def wifi_status(status):
    socketio.emit('wifi_status', status)

def show_settings(settings):
    socketio.emit('show_settings', settings)

def alert(msg):
    socketio.emit('alert', msg)

def quit():
    socketio.emit('quit', '')
    
def run(collection, event_callback, port):
    global dinge
    global callback
    
    dinge = collection
    #dinge.print_all()
    callback = event_callback
    # app.run(debug=True, host='0.0.0.0',port=8080)
    socketio.run(app, debug=False, use_reloader=False, host='0.0.0.0', port=port)
