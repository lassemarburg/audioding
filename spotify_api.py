"""use spotify api spotipy module to control audio playback on raspotify device
see https://spotipy.readthedocs.io/en/latest/

SPOTIPY_CLIENT_ID and SPOTIPY_CLIENT_SECRET need to be stored as environment variables
"""
import spotipy, time, requests, os
class SpotifyAPI(object):
    def __init__(self, config, callback):
        self.scope = 'user-library-read,user-read-playback-state,user-modify-playback-state,playlist-read-private,playlist-read-collaborative,playlist-modify-private,playlist-modify-public'
        self.authenticated = False
        self.config = config
        self.callback = callback
        self.device = None
        self.volume = 0
        self.connected = False

        self.ding = None

    def init(self):
        try:
            return self.authentication('audioding_user')
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.exceptions.HTTPError) as e:
            print("ERROR Could not connect to spotify")
            print(e)
            return False
        except (spotipy.oauth2.SpotifyOauthError) as e:
            print(e.msg)
            self.auth_error(e, False)
            return False
        except (spotipy.exceptions.SpotifyException) as e:
            self.error( str(e), "SpotifyException", False)
            return False

    def auth_error(self, e, alert=True):
        if hasattr(e, "error_description"):
            error_return = self.error(e.error_description, "SpotifyAuthError", alert)
        
        self.authenticated = False
        self.connected = False

        if e.error == "invalid_grant":
            if self.deleteToken():
                return self.authentication(self.username, True)
            else:
                return error_return
        else:
            return error_return

    def connect_error(self):
        return self.error('Server konnte nicht erreicht werden. Überprüfe die Internetverbindung.', "ConnectError")

    def no_device_error(self):
        return self.error('Es konnte kein Spotify Player gefunden werden. Melde das Audioding in den Spotify Einstellungen als Audioplayer an.', "NoDeviceError")

    def error(self, msg, type="", alert=True):
        print(msg)
        if alert:
            self.callback('alert',{'type':'danger','title':'Spotify Fehler','message':str(msg)})
        return {'error':{'message':str(msg),'type':type}}
    
    def deleteToken(self):
        if hasattr(self, "username"):
            if os.path.exists(".cache-%s" % self.username):
                print("Delete current cashed token")
                os.remove(".cache-%s" % self.username)
                return True
            else:
                print("Cashed token file '.cache-%s' does not exist" % self.username)
        else:
            print("Can not delete token. No username defined")
        return False

    def authentication(self, username, show_dialog=False):
        """try to authenticate spotify user


        Args:
            username (str): local username for cache file
            show_dialog (bool, optional): If True show login dialog anyways even if already authenticated. Defaults to False.

        Returns:
            obj: information about user or spotify login url
        """
        self.auth_manager = spotipy.oauth2.SpotifyOAuth(
            username=username, 
            scope=self.scope,
            redirect_uri=self.config["spotify_redirect_url"],
            client_id=self.config["spotify_client_id"],
            client_secret=self.config["spotify_client_secret"],
            show_dialog=show_dialog
            )
        self.spotify = spotipy.Spotify(auth_manager=self.auth_manager,requests_timeout=10,retries=20)

        self.username = username
        token = self.auth_manager.cache_handler.get_cached_token()
        # token = self.auth_manager.get_cached_token()
        validation = self.validate_token(token)

        self.connected = True

        device_retry = 0
        while self.authenticated and device_retry <= 10 and not self.device:
            if device_retry > 0:
                print("Could not find spotify device. Retry %i" % device_retry)
            self.get_device()
            device_retry += 1
            time.sleep(3)

        return validation

    def get_auth_url(self):
        return self.auth_manager.get_authorize_url()

    def validate_token(self, token, get_status=True):
        """see if auth token is valid if any

        if no token or token expired return auth url

        Args:
            token (obj): spotify user auth token

        Returns:
            obj: information about user or spotify login url
        """
        if token:
            print("token expired: %s" % self.auth_manager.is_token_expired(token))
            if self.auth_manager.is_token_expired(token):
                self.authenticated = False
                return {"url":self.auth_manager.get_authorize_url()}
            
            self.authenticated = True
            if get_status:
                print("Eingeloggt als %s" % self.spotify.me()["display_name"])
                return {"authenticated":True, "user":self.spotify.me()}
            else:
                return {"authenticated":True}
            
        else:
            self.authenticated = False
            return {"url":self.auth_manager.get_authorize_url()}

    def device_login(self, credentials):
        raspotify_options = "sed -i 's/^OPTIONS=.*/OPTIONS=\"--zeroconf-port %s --username %s --password %s\"/g' /etc/default/raspotify" % (self.config['zeroconf_port'], credentials['username'], credentials['password'])
        options = os.popen(raspotify_options).read()
        print(options)
        return self.raspotify('restart')


    def raspotify(self, cmd):
        ret = os.popen('sudo service raspotify %s' % cmd).read()
        print(ret)
        dev = None
        for i in range(3):
            time.sleep(1)
            dev = self.get_device()
            if dev is not None and dev['is_local_device']:
                break
        return dev

    def get_device(self):
        """search for audioding in spotify devices

        pick first device in list if audioding could not be found.

        Update self.device

        Returns:
            dict: device properties (name, id ...) or None if no device could be found
        """
        devices = self.spotify.devices()

        #print(devices)
        for device in devices['devices']:
            if device['name'] == 'audioding':
                print("Audioding spotify device found: ")
                print(device)
                device['is_local_device'] = True
                self.device = device
                self.volume = device['volume_percent']/100
                return device
                
        if len(devices['devices']):
            print("No Audioding Spotify Device. Assign %s instead." % devices['devices'][0]['name'])
            device['is_local_device'] = False
            self.device = device
            self.volume = device['volume_percent']/100
            return devices['devices'][0]
        
        print("No spotify connect device found.")
        self.device = None
        return None

    def assign(self, ding, content, volume):
        """link ding to spotify playlist

        Args:
            ding (collection.Ding): the ding playlist will be assigned to
            content (dict): playlist attributes
        """
        ding.setup('spotify', {'volume':volume,'playlist':content["playlist"]["id"], 'uri':content["playlist"]["uri"], 'name':content["playlist"]["name"], 'options':['reload','clear']})
        self.callback('established',ding)
        return {'response':'spotify playlist assigned'}

    def load(self, ding):
        """make playlist update to keep track of recent changes

        store ding for future commands and status changes

        Args:
            ding (collection.Ding): ding that stores information about current track, position etc.
        """
        self.ding = ding
        try:
            self.get_device()
            self.playlist_update()
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as e:
            return self.connect_error()
        except (spotipy.oauth2.SpotifyOauthError) as e:
            return self.auth_error(e)
        except (spotipy.exceptions.SpotifyException) as e:
            return self.error(str(e), "SpotifyException")

    def stop(self):
        try:
            if self.playback_status():
                self.command({"playback":"pause"})
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as e:
            return self.connect_error()
        except (spotipy.oauth2.SpotifyOauthError) as e:
            return self.auth_error(e)
        except (spotipy.exceptions.SpotifyException) as e:
            return self.error(str(e), "SpotifyException")
        self.ding = None

    def play(self, ding):
        """begin audio playback from stored position

        assign ding for future commands and status changes

        Args:
            ding (collection.Ding): ding that stores information about current track, position etc.
        """
        self.ding = ding

        if not self.ding.current_track:
            self.ding.set('current_track', self.ding.tracks[0])

        self.play_track(self.ding.current_track['uri'], self.ding.play_position)

        if hasattr(self.ding, 'volume'):
            set_volume_retry = 0
            volume_set = ""
            forgiving = True
            while set_volume_retry <= 3 and not volume_set:
                if set_volume_retry == 3:
                    forgiving = False

                if self.set_volume(self.ding.volume, forgiving) == "successful":
                        volume_set = True
                else:
                    set_volume_retry += 1
                    print("Could not set volume after play command. Retry %i" % set_volume_retry)
                    time.sleep(1)

        
    def play_track(self, track_uri, pos=0):
        if self.device is None:
            return self.no_device_error()
        try:
            self.spotify.start_playback(device_id=self.device['id'], context_uri=self.ding.uri, offset={"uri":track_uri}, position_ms=pos*1000)

            self.callback("play_status", self.get_status())
        except spotipy.exceptions.SpotifyException as e:
            print("Could not play spotify track %s" % track_uri)
            print(e)
            self.error("Could not play spotify track %s\n%s"  % (track_uri,str(e)))
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as e:
            return self.connect_error()
        except (spotipy.oauth2.SpotifyOauthError) as e:
            return self.auth_error(e)

    def playlist_update(self):
        """get currrent ding playlist data from spotify and update ding

        collects tracks from playlist tracks.items and merges track property into ding.tracks

        if spotify reports user is playing a track in this playlist. set current_track to this track.

        set first track in list to be current_track
        """
        playlist = self.get_playlist_essentials(self.ding.playlist)
        self.ding.set('tracks', playlist['tracks'])

        self.playback_status()

    def get_status(self):
        """get current playback status from sportify api

        sleep 600ms before getting status from spotify. 
        The play command might take a while so that without the delay playback status might not be accurate.

        Returns:
            dict: information about current track, playback and volume
        """
        #  or maybe: self.spotify.current_user_playing_track()
        # or maybe: self.spotify.currently_playing()
        if self.ding:
            try:
                time.sleep(.6)
                is_playing = self.playback_status()
                pause = True
                if is_playing:
                    pause = False
                return {"track":self.ding.current_track, "position":self.ding.play_position, "pause":pause, "volume":self.volume}
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, spotipy.oauth2.SpotifyOauthError, spotipy.exceptions.SpotifyException) as e:
                return {"position":0.0, "length":0.0, "pause":False, "volume":self.volume}
       
        else:
            return {"position":0.0, "length":0.0, "pause":False, "volume":self.volume}

    def playback_status(self):
        playback = self.spotify.current_playback()
        if playback:
            if playback['item'] is not None:
                print("current spotify playback is %s on %s" % (playback['item']['name'], playback['device']['name']))
            
            # 'device' does not always exist it seems
            if playback['device'] is None:
                print("Warning: playback has no device")
            elif self.device is None:
                print("Warning: device is of type None")
            elif playback['device']['id'] == self.device['id']:
                self.volume = playback['device']['volume_percent']/100

            if playback['context'] is None:
                print("Warning: playback has no context")
                return False
            
            if self.ding is None:
                print("Warning: Ding is of type None")
                return False

            if playback['context']['uri'] == self.ding.uri:
                print("This ding playlist is active on spotify. Playing: %d" % playback['is_playing'])
                print(playback['item']['uri'])
                self.ding.set('current_track', self.ding.getTrack('id', playback['item']['uri'])) # self.track_essentials(playback['item']))
                self.ding.set('play_position', playback['progress_ms']/1000)

                return playback['is_playing']
            
            print("spotify playback is not the current ding playlist.")
            return False

        return False

    def command(self, msg):
        try:
            if "auth_code" in msg:
                self.deleteToken()
                token = self.auth_manager.get_access_token(msg["auth_code"])
                status = self.validate_token(token)
                print("spotify token stored")
                print(status)
            elif "login" in msg:
                return self.authentication(msg["login"])
            elif "device_login" in msg:
                return self.device_login(msg["device_login"])
            elif "change_login" in msg:
                # self.authentication(msg["change_login"], True)
                url = "%s%s" %(self.auth_manager.get_authorize_url(), "&show_dialog=true")
                return {"url":url}
            elif self.authenticated:
                if "auth_status" in msg:
                    return {"authenticated":True, "user":self.spotify.me(), "device":self.get_device()}
                if "playback" in msg:
                    self.playback(msg["playback"])
                if "switch_track" in msg:
                    self.switch_track(msg["switch_track"])
                if "set_volume" in msg:
                    self.set_volume(msg["set_volume"])
                if "volume_up" in msg:
                    self.volume_up(msg["volume_up"])
                if "volume_down" in msg:
                    self.volume_down(msg["volume_down"])
                if 'backward' in msg:
                    self.backward(msg['backward'])
                if 'position' in msg:
                    self.set_position(msg['position'])
                if 'forward' in msg:
                    self.forward(msg['forward'])
                if "playlists" in msg:
                    # playlist value is offset
                    return self.spotify.current_user_playlists(offset=msg["playlists"])
                if "get_playlist" in msg:
                    return self.spotify.playlist(msg["get_playlist"],)
                if "get_playlist_essentials" in msg:
                    return self.get_playlist_essentials(msg["get_playlist_essentials"])
                    #return self.spotify.playlist(playlist_id=msg["get_playlist_essentials"]["id"], fields=msg["get_playlist_essentials"]["fields"]) # ,'fields':'track(artists,album(name),duration_ms,uri,name))' ,fields=msg["get_playlist_tracks"]["fields"]
                if "currently_playing" in msg:
                    return self.spotify.current_playback()
                if "tracklist" in msg:
                    return self.tracklist(msg)
                if "get_status" in msg:
                    return self.get_status()
            elif not self.connected:
                self.authentication('audioding_user')
            elif "auth_status" in msg:
                return {"authenticated":False}
            else:
                print("Spotify user is not authenticated!")
                self.error('du bist nicht als spotify user eingeloggt')
                return {"error":"not authenticated"}
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.exceptions.HTTPError) as e:
            return self.connect_error()
        except (spotipy.oauth2.SpotifyOauthError) as e:
            return self.auth_error(e)
        except (spotipy.exceptions.SpotifyException) as e:
            return self.error(str(e), "SpotifyException")

    def playback(self, msg):
        if self.device is None:
                return self.no_device_error()
        print(msg)
        try:
            if msg == "pause":
                self.spotify.pause_playback()
            if msg == "resume":
                self.spotify.start_playback(device_id=self.device['id'], context_uri=self.ding.uri, offset={"uri":self.ding.current_track['uri']}, position_ms=self.ding.play_position*1000)
            if msg == "skip_end":
                self.spotify.next_track()
            if msg == "skip_start":
                self.playback_status()
                if self.ding.play_position < 5.0:
                    self.spotify.previous_track()
                else:
                    self.set_position(0.0)
        except spotipy.exceptions.SpotifyException as e:
            print("Could not call playback method %s" % str(msg))
            print(e)
            return {"error":{"type":"SpotifyException","message":str(e)}}


        self.callback("play_status", self.get_status())

    def switch_track(self, track):
        print("change track to %s" % track)
        if self.ding is not None:
            self.play_track(track, 0)
        else:
            print("could not switch to track %s. No Ding found for audio player." % track)

    def set_position(self, pos):
        if pos < 0:
            pos = 0

        print("Set to play position %f" % int(pos*1000))
        self.play_track(self.ding.current_track['uri'], pos)
        # self.spotify.seek_track(int(pos*1000), device_id=self.device['id'])

    def forward(self, seconds):
        self.playback_status()
        self.set_position(self.ding.play_position + seconds)

    def backward(self, seconds):
        self.playback_status()
        self.set_position(self.ding.play_position - seconds)
    
    def set_volume(self, value, forgiving=False):
        if self.device is None:
            return self.no_device_error()
                
        if value >= 1:
            value = 1
        elif value <= 0:
            value = 0
        print("spotify volume to %f on device %s" % (value, self.device['name']))
        self.ding.set("volume", value)
        try:
            self.spotify.volume(round(value*100))
            self.callback("play_status", self.get_status())
            return "successful"
        except spotipy.exceptions.SpotifyException as e:
            print("Could not set volume for device %s" % self.device)
            print(e)
            if not forgiving:
                self.error("Could not set volume for device %s\n%s"  % (self.device,str(e)))
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.exceptions.HTTPError) as e:
            return self.connect_error()
        except (spotipy.oauth2.SpotifyOauthError) as e:
            return self.auth_error(e)

    def volume_up(self, value):
        self.update_volume()
        self.set_volume(self.volume + value)
    
    def volume_down(self, value):
        self.update_volume()
        self.set_volume(self.volume - value)

    def update_volume(self):
        self.get_device()

    def tracklist(self, msg):
        if "reload" in msg:
            self.playlist_update()

    def add_track(self, track):
        """
        https://spotipy.readthedocs.io/en/latest/?highlight=position#spotipy.client.Spotify.playlist_add_items
        """
        self.spotify.playlist_add_items(playlist_id, items, position=None)

    def reorder(self, options):
        """
        https://spotipy.readthedocs.io/en/latest/?highlight=position#spotipy.client.Spotify.playlist_reorder_items
        """
        if options['track']['from']-1 > options['track']['to']:
            options['track']['to'] -= 1
        try:
            self.spotify.playlist_reorder_items(self.ding.playlist, options['track']['from']-1, options['track']['to'], range_length=1, snapshot_id=None)
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as e:
            return self.connect_error()

    def get_playlist_essentials(self, playlist_id):
        """get playlist data and compress tracklist information

        tracks info as in Spotify.track_essentials

        Args:
            playlist_id (str): spotify playlist id

        Returns:
            dict: reduced json containing only basic information about the playlist
        """
        playlist = self.spotify.playlist(playlist_id=playlist_id,fields='id,description,name,tracks(items(track(name,album(name),artists(name),uri,duration_ms)))')
        
        tracks_items = self.spotify.playlist_items(playlist_id=playlist_id)
        
        tracks = []
        for track in tracks_items['items']:
            # track = track['track']
            track = self.track_essentials(track['track'])
            # track['album'] = track['album']['name']
            # artists = []
            # for artist in track['artists']:
            #     artists.append(artist['name'])
            # track['artists'] = artists
            tracks.append(track)

        playlist['tracks'] = tracks
        return playlist

    def track_essentials(self, track):
        """pick basic information from extensive track spotify track info

        "album" (str)
        "artists" (list)
        "duration" (float) duration in seconds (spotify duration is in ms)
        "name" (str)
        "uri" (str)

        Copy "uri" to "id" to allow identification in web client.

        Args:
            track (dict): all information spotify returned about track

        Returns:
            dict: condensed track info
        """
        track['album'] = track['album']['name']
        track['duration'] = track['duration_ms']/1000
        artists = []
        for artist in track['artists']:
            artists.append(artist['name'])
        track['artists'] = artists
        track['id'] = track['uri']
        return track

