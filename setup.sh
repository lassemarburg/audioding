#!/bin/sh
# setup.sh
# Run raspberry pi setup for audioding
# Enabling of auto login requires to run this in root (sudo)

if [ "$#" -ge 1 ]
then
    name=$1
else
    name="audioding"
fi

echo setup as $name

# Run apt upgrade
echo run system update ------------
sudo apt update
sudo apt upgrade

# Clone respoitory
echo install audioding and dependencies ------------

sudo apt install git
git clone https://gitlab.com/lassemarburg/audioding.git

## Install Audioding
# Install python packages
sudo apt install python3-dev
# sudo apt install python3-rpi.gpio

git clone https://github.com/lthiery/SPI-Py.git
cd SPI-Py
sudo python3 setup.py install
cd ..

sudo apt install python3-pygame

sudo apt install python3-pip

sudo pip3 install mutagen

sudo pip3 install flask
sudo pip3 install flask-socketio

sudo pip3 install spotipy --upgrade

# Install service and activate autostart
sudo cp audioding/audioding.sh /etc/init.d/audioding
sudo chmod +x /etc/init.d/audioding
sudo update-rc.d audioding defaults

## Run setup for raspberrypi
echo adapt raspberry pi setup ------------
# Activate SPI
sudo sed -i "s/#dtparam=spi=on/dtparam=spi=on/" /boot/config.txt

# Setup auto-login
systemctl set-default multi-user.target
ln -fs /lib/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@tty1.service
cat > /etc/systemd/system/getty@tty1.service.d/autologin.conf << EOF 
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin pi --noclear %I \$TERM 
EOF

# Boot and shutdown setup
sudo cp audioding/gpio-shutoff /lib/systemd/system-shutdown/gpio-shutoff
sudo chmod +x /lib/systemd/system-shutdown/gpio-shutoff

sudo sed -i '$a# audioding custom gpio settings: set power status led to blink output' /boot/config.txt
sudo sed -i '$agpio=6=op,dh' /boot/config.txt

# Install Raspotify
echo install raspotify ------------
curl -sL https://dtcooper.github.io/raspotify/install.sh | sh

sudo sed -i "s/#DEVICE_NAME=\"raspotify\"/DEVICE_NAME=\"$name\"/g" /etc/default/raspotify
sudo sed -i '$aOPTIONS=\"--zeroconf-port 5555\"' /etc/default/raspotify
sudo sed -i '$aVOLUME_ARGS=\"--initial-volume=30\"' /etc/default/raspotify

# Audio interface setup
echo setup sound driver ------------
curl -sS https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/i2samp.sh | bash

# WiFi Hotspot setup
echo install hotspot setup ------------

sudo apt install hostapd
sudo systemctl unmask hostapd
sudo apt install dnsmasq

sudo systemctl disable hostapd
sudo systemctl disable dnsmasq

# sudo DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent

sudo sed -i '$anohook wpa_supplicant' /etc/dhcpcd.conf

sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo mv audioding/hotspot/dnsmasq.conf /etc/dnsmasq.conf

sudo rfkill unblock wlan

sudo mv audioding/hotspot/hostapd.conf /etc/hostapd/hostapd.conf

sudo sed -i "s/#DAEMON_CONF=\"\"/DAEMON_CONF=\"\/etc\/hostapd\/hostapd.conf\"/g" /etc/default/hostapd

sudo mv audioding/hotspot/autohotspot.service /etc/systemd/system/autohotspot.service

sudo systemctl enable autohotspot.service

sudo mv audioding/hotspot/autohotspot /usr/bin/autohotspot

sudo chmod +x /usr/bin/autohotspot

# Change Hostname

echo change hostname ------------

sudo sed -i "s/raspberrypi/$name/g" /etc/hosts
sudo sed -i "\$a192.168.4.1 $name" /etc/hosts

sudo sed -i "s/raspberrypi/$name/g" /etc/hostname

sudo sed -i "s/domain=wlan/domain=$name/g" /etc/dnsmasq.conf
sudo sed -i "s/address=\/gw.wlan\/192.168.4.1/address=\/$name\/192.168.4.1/g" /etc/dnsmasq.conf

sudo sed -i "s/ssid=audioding/ssid=$name/g" /etc/hostapd/hostapd.conf