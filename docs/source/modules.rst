audioding
=========

.. toctree::
   :maxdepth: 4

   MFRC522
   app
   collection
   control
   player
   reader
   server
   spotify_api
   wifi
