MFRC522 module
==============

.. automodule:: MFRC522
   :members:
   :undoc-members:
   :show-inheritance:
