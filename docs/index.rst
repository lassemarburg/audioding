.. Audioding documentation master file, created by
   sphinx-quickstart on Sat Aug 15 16:33:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Audioding's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   source/readme.rst
   source/modules.rst
   source/webview/files.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
