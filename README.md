Audioding
=========

Instalation
------------

### Raspberry Pi OS light

#### Linux / macOS

Lade dir die aktuelle version von Raspberry Pi OS **lite** auf raspberrypi.com herunter:

https://www.raspberrypi.com/software/operating-systems/

Wenn du die .zip datei heruntergeladen hast, entpacke das image (passe den Dateinamen ggf. an):

`$ unzip 2021-10-30-raspios-bullseye-armhf-lite.zip`

Lege die sd Karte ein und bespiele sie mit dem image (passe den Dateinamen des image und den Device pfad der SD KArte an):

`$ sudo dd bs=4M if=2020-02-13-raspbian-buster.img of=/dev/sda status=progress conv=fsync`

Aktiviere ssh indem du eine leere Datei mit dem Namen `ssh` im boot laufwerk der sd karte anlegst:

`$ nano /media/<username>/boot/ssh`

[ctrl-o] [Return] [ctrl-x]

Lege den Zugang zu deinem Heim Netzwerk in der wlan config Datei an:

`$ nano /media/<username>/boot/wpa_supplicant.conf`

Füge der Datei folgenden Inhalt hinzu. Ersetze `NETWORK-NAME` mit deinem W-Lan und `NETWORK-PASSWORD` mit deinem W-Lan Passwort:

```
country=DE
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="NETWORK-NAME"
    psk="NETWORK-PASSWORD"
}
```
[ctrl-o] [Return] [ctrl-x]

siehe auch: https://desertbot.io/blog/headless-raspberry-pi-4-ssh-wifi-setup

#### Windows

Lade den Raspberry Pi Imager für Windows herunter: https://www.raspberrypi.com/software/

Führe die imager_x.exe Datei aus und installiere den Imager.

Starte den Raspberry Pi OS Imager und klicke auf `OS WÄHLEN`. Wähle die zweite Option `Raspberry Pi OS (other)` aus und anschließend `Raspberry Pi OS Light (32-bit)`

Wähle deine SD Karte aus mit `SD-KARTE WÄHLEN`.

Klicke auf `SCHREIBEN`.

Wenn der Schreibvorgang beendet ist, öffne den Datei Explorer. Es gibt ein neues Laufwerk mit dem namen `boot`. 

Erstelle im Hauptverzeichnis von `boot` eine neue Datei `ssh` ohne Dateiendung.

Erstelle im selben Verzeichnis eine Datei mit dem namen `wpa_supplicant.conf`. Öffne die Datei mit einem texteditor und füge `den folgenden Text ein:

```
country=DE
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="NETWORK-NAME"
    psk="NETWORK-PASSWORD"
}
```

Ersetze `NETWORK-NAME` mit deinem W-Lan und `NETWORK-PASSWORD` mit deinem W-Lan Passwort.

Lege die SD-Karte in den Raspberry Pi ein und verbinde die Stromversorgung. Öffne jetzt eine Powershell um die Installation auf dem raspberry pi per ssh weiterzuführen. Führe folgendes Kommando in der Powershell aus um dich zu verbinden:

`$ ssh pi@raspberrypi`

### Audioding server

verbinde dich über ssh mit dem raspberry pi:

`$ ssh pi@raspberrypi`

Das Passwort ist `raspberry`

Führe ein Update auf dem raspberry pi aus:

`$ sudo apt update`

`$ sudo apt upgrade`

Installiere git auf dem raspberry pi:

`$ sudo apt install git` 

Klone dieses repository mit git:

`git clone https://gitlab.com/lassemarburg/audioding.git`

### Python packages


`$ sudo apt install python3-dev`

`$ sudo apt install python3-rpi.gpio` <- ggf.

```
$ git clone https://github.com/lthiery/SPI-Py.git
$ cd SPI-Py
$ sudo python3 setup.py install
```
`$ sudo apt install python3-pygame`

`$ sudo apt install python3-pip`

`$ sudo pip3 install mutagen`

`$ sudo pip3 install flask`

`$ sudo pip3 install flask-socketio`

### SPI aktivieren

entweder über

`$ sudo raspi-config`

oder mit (in diesem fall ist ein reboot notwendig)

`sudo sed -i "s/#dtparam=spi=on/dtparam=spi=on/" /boot/config.txt`

### Autostart einrichten

Verschiebe die audioding service Datei in den init.d Ordner

`$ sudo cp audioding/audioding.sh /etc/init.d/audioding`

Mache die Datei ausführbar

`$ sudo chmod +x /etc/init.d/audioding`

Füge den audioding service zum Autostart hinzu

`$ sudo update-rc.d audioding defaults`

Ändere das Login verhalten, sodass sich der user beim start ohne passwort eingabe anmeldet:

entweder in raspi-config

`$ sudo raspi-config`

`3 Boot options` -> `B1 Desktop / CLI` -> `B2 Console Autologin` -> `<Ok>`

oder

`$ systemctl set-default multi-user.target`

`$ ln -fs /lib/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@tty1.service`

`$ cat > /etc/systemd/system/getty@tty1.service.d/autologin.conf << EOF [Service] ExecStart=ExecStart=-/sbin/agetty --autologin pi --noclear %I \$TERM EOF;;`

### Boot und Shutdown einrichten

Verschiebe die shutdown Datei für den Onoff shim shutdown und mache sie ausführbar

`$ sudo cp gpio-shutoff /lib/systemd/system-shutdown/gpio-shutoff`
`$ sudo chmod +x /lib/systemd/system-shutdown/gpio-shutoff`

Ergänze die Datei /boot/config.txt um die power status LED bei boot zu aktivieren
`$ sudo sed -i '$a# audioding custom gpio settings: set power status led to blink output' /boot/config.txt`
`$ sudo sed -i '$agpio=6=op,dh' /boot/config.txt`

### Audio Interface

#### Adafruit Bonnet Speaker
wenn du das adafruit speaker bonnet benutzt installiere die I2C Schnittstelle:

`$ curl -sS https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/i2samp.sh | bash`

Details siehe https://learn.adafruit.com/adafruit-speaker-bonnet-for-raspberry-pi/raspberry-pi-usage

#### HiFi Berry Amp v1
Wenn du die Installation für den Adafruit Bonnet Speaker bereits ausgeführt hast brauchst du die folgende Einstallung nicht mehr vornehmen.

`$ sudo nano /boot/config.txt`

HiFiberry am Ende der Datei eintragen:

`dtoverlay=hifiberry-dac`

Um HiFiberry als default Soundkarte auszuwählen, folgende Datei erstelln:

`nano /etc/asound.conf`

und diese zwei Zeilen eintragen:

```
defaults.pcm.card 1
defaults.ctl.card 1
```

### Bootstrap und jQuery (wenn nicht im repository)

`$ wget -P static/external/ -O jquery.min.js https://code.jquery.com/jquery-3.4.1.min.js`

`$ wget -P static/external/ https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css`

`$ wget -P static/external/ https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js`

### Wlan Hotspot

Setze den Raspberry Pi als WiFi Zugangspunkt auf. Folge der Anleitung im folgenden link mit ein paar Ausnahmen

https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-routed-wireless-access-point

`$ sudo apt install hostapd`

`$ sudo systemctl unmask hostapd`

`$ sudo apt install dnsmasq`

Deaktiviere die installierten Dienste anstatt sie zu aktivieren:

`sudo systemctl disable hostapd`

`sudo systemctl disable dnsmasq`

Installiere `netfilter-persistent` (muss noch getestet werden!)

`sudo DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent`

Im Kapitel "Define the wireless interface IP configuration" füge nur diese Zeile am Ende der Datei /etc/dhcpcd.conf hinzu: `nohook wpa_supplicant`

`$ sudo sed -i '$anohook wpa_supplicant' /etc/dhcpcd.conf`

Überspringe das Kapitel "Enable routing and IP masquerading".

Im Kapitel "Configure the DHCP and DNS services for the wireless network" erstelle die neue /etc/dnsmasq.conf Datei mit folgendem Inhalt:

`$ sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig`

`$ sudo nano /etc/dnsmasq.conf`

```
interface=wlan0 # Listening interface
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
                # Pool of IP addresses served via DHCP
domain=wlan     # Local wireless DNS domain
address=/audioding/192.168.4.1
                # Alias for this router
```

> oder `$ sudo mv ./hotspot/dnsmasq.conf /etc/dnsmasq.conf`

Lediglich gw.wlan sollte durch audio.ding ersetzt sein.

`$ sudo rfkill unblock wlan`

In Kapitel "Configure the access point software" erstelle die /etc/hostapd/hostapd.conf Datei mit folgendem Inhalt:

```
country_code=DE
interface=wlan0
ssid=audioding
hw_mode=g
channel=7
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=funkding
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

> oder: `sudo mv ./hotspot/hostapd.conf /etc/hostapd/hostapd.conf`

Ergänze die Einstellungen mit folgender Anleitung:

https://www.raspberryconnect.com/projects/65-raspberrypi-hotspot-accesspoints/158-raspberry-pi-auto-wifi-hotspot-switch-direct-connection

In der Datei `/etc/default/hostapd` ändere die Zeile `#DAEMON_CONF=""` in `DAEMON_CONF="/etc/hostapd/hostapd.conf"`

`$ sudo sed -i "s/#DAEMON_CONF=\"\"/DAEMON_CONF=\"\/etc\/hostapd\/hostapd.conf\"/g" /etc/default/hostapd`

Verschiebe die Datei autohotspot.service nach /etc/systemd/system/ und aktiviere den service

`$ sudo mv ./hotspot/autohotspot.service /etc/systemd/system/autohotspot.service`

`$ sudo systemctl enable autohotspot.service`

Verschiebe die Datei autohotspot nach /usr/bin/ und mache sie ausführbar

`$ sudo mv ./hotspot/autohotspot /usr/bin/autohotspot`

`$ sudo chmod +x /usr/bin/autohotspot`


Überspringe den anfang und beginne mit dem Kapitel "autohotspot service file"

Ändere die IP `10.0.0.5` in der Datei /usr/bin/autohotspot in `192.168.4.1`


Ändere den Hostname in allen relevanten files:

`$ sudo sed -i "s/raspberrypi/audioding/g" /etc/hosts`
`$ sudo sed -i '$a192.168.4.1 audioding' /etc/hosts`
`$ sudo sed -i '$a127.0.0.1 audioding.app' /etc/hosts`

`$ sudo sed -i "s/raspberrypi/audioding/g" /etc/hostname`

`$ sudo sed -i "s/domain=wlan/domain=audioding/g" /etc/dnsmasq.conf`
`$ sudo sed -i "s/address=\/gw.wlan\/192.168.4.1/address=\/audioding.app/192.168.4.1/g" /etc/dnsmasq.conf`

### Spotify

#### Raspotify 

Installiere [raspotify](https://github.com/dtcooper/raspotify) um audioding als Spotify Gerät einzurichten

`sudo apt-get -y install curl && curl -sL https://dtcooper.github.io/raspotify/install.sh | sh`

Modifiziere die raspotify config Datei um den Namen anzupassen, den Start Volume zu ändern und den zeroconf server auf den port `5555` festzulegen

> NICHT GETESTET und muss noch in setup.sh angepasst werden

`$ sudo sed -i "s/#DEVICE_NAME=\"raspotify\"/DEVICE_NAME=\"audioding\"/g" /etc/raspotify/conf`
`$ sudo sed -i "aOPTIONS=\"--zeroconf-port 5555\"" /etc/raspotify/conf`
`$ sudo sed -i "aVOLUME_ARGS=\"--initial-volume=30\"" /etc/raspotify/conf`

#### Raspotify Pi Zero v1 und Pi v1

> see https://github.com/dtcooper/raspotify/wiki/Raspotify-on-Pi-v1's-and-Pi-Zero-v1.x

Install curl and download raspotify_0.31.8.1

`sudo apt-get -y install curl && curl -O -sL https://github.com/dtcooper/raspotify/releases/download/0.31.8.1/raspotify_0.31.8.1.librespot.v0.3.1-54-gf4be9bb_armhf.deb`

`Install raspotify_0.31.8.1`

sudo apt install ./raspotify_0.31.8.1.librespot.v0.3.1-54-gf4be9bb_armhf.deb

Modifiziere die raspotify config Datei um den Namen anzupassen, den Start Volume zu ändern und den zeroconf server auf den port `5555` festzulegen

`$ sudo sed -i "s/#DEVICE_NAME=\"raspotify\"/DEVICE_NAME=\"audioding\"/g" /etc/default/raspotify`
`$ sudo sed -i "aOPTIONS=\"--zeroconf-port 5555\"" /etc/default/raspotify`
`$ sudo sed -i "aVOLUME_ARGS=\"--initial-volume=30\"" /etc/default/raspotify`

#### Spotipy

Installiere das python modul [spotipy](https://spotipy.readthedocs.io/en/latest/)

`$ sudo pip3 install spotipy --upgrade`

Ggf. braucht es auch flask-session (derzeit nicht)

`$ pip3 install flask-session`

Erstelle die datei `config.json` mit deinen Spotify Developer credentials:

`$ nano config.json`

```
{
    "spotify_client_id":"yourclientid",
    "spotify_client_secret":"yourclientsecret" 
}
```


#### Spotify alternative (überholt)

Installiere das python modul [spotify.py](https://pypi.org/project/spotify/)

`$ sudo pip3 install spotify`

Hier die Dokumentation zu spotify.py: https://spotifypy.readthedocs.io/en/latest/index.html

Und hier die git homepage: https://github.com/mental32/spotify.py


### Captive Portal (überholt)

Ein captive portal ist zunächst nicht hilfreich. Es bringt zu viele extras und Probleme mit sich. Kann wieder aus der Kiste geholt werden falls sich eine gute Variante findet.

>Ein Captive Portal mit Nodogsplash aufsetzen das beim Verbinden mit dem Netzwerk eine Seite aufruft:

>https://pimylifeup.com/raspberry-pi-captive-portal/
https://nodogsplash.readthedocs.io/en/latest/customize.html

>Die Nodogsplash Audioding startseite kopieren:

>`$ sudo cp ./portal/splash.html /etc/nodogsplash/htdocs/splash.html`
>`$ sudo cp ./portal/splash.css /etc/nodogsplash/htdocs/splash.css`

Hardware
---------

### NFC Reader

Folge den Anweisungen dieser Tutorials für den RC522 NFC Reader:

<https://www.raspberrypi-spy.co.uk/2018/02/rc522-rfid-tag-read-raspberry-pi/>

<https://tutorials-raspberrypi.de/raspberry-pi-rfid-rc522-tueroeffner-nfc/>


Die python  NFC library ist nicht auf aktuellstem Stand und muss wegen änderungen an der SPI library angepasst werden. Das Problem ist hier diskutiert und eine Lösung angeboten, die für Audioding übernommen wurde:

<https://github.com/mxgxw/MFRC522-python/issues/69#issuecomment-466571581>


Test
----

Nutze `place` und `remove` in der Komandozeile um den reader zu simulieren.
