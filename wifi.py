import os, json, re, time
try:
    import httplib
except:
    import http.client as httplib

device = "wlan0"
wpa_file = "/etc/wpa_supplicant/wpa_supplicant.conf"

#
# unused function
#
def load():
    with open("networks.json") as json_file:
        global networks
        networks = json.load(json_file)

#
# unused function
#
def get_networks():
    return networks

#
# change inerface to something else then wlan0
# e.g. for debugging on an ubuntu machine
#
def interface(wl):
    global device
    device = wl
    if not device == "wlan0":
        global wpa_file
        wpa_file = "wpa_supplicant.conf"

def connect(network):
    """
    add or replace wifi info in wpa_supplicant file and restart autohotspot script
    
    Returns:
            bool: True if internet connection was established
    """
    if "PSK" in network:
        wpa_supplicant('{\n\tssid="%s"\n\tpsk="%s"\n}' % (network["SID"], network["PSK"]))
    else:
        wpa_supplicant('{\n\tssid="%s"\n\tkey_mgmt=NONE\n}' % (network["SID"]))

    print("Connect to wifi %s." % network["SID"])
    cmd("sudo /usr/bin/autohotspot")

    for i in range(3):
        if is_online():
            return True
        time.sleep(1)

    return False
    
def disconnect():
    """
    add or replace wifi info in wpa_supplicant file and restart autohotspot script
    
    Returns:
            bool: True if internet connection was disconnected
    """
    wpa_supplicant('{\n}')
    print("Disconnect wifi. Restart audioding Hotspot.")
    cmd("sudo /usr/bin/autohotspot")

    for i in range(3):
        if not is_online():
            return True
        time.sleep(1)

    return False

def wpa_supplicant(network):
    wpa_replace = ""
    with open(wpa_file, "r") as wpa_supplicant_file:
            
            wpa_replace = re.sub(r'{(.*)}', network, wpa_supplicant_file.read(), flags=re.S)
            print(wpa_replace)

    with open(wpa_file, "w") as wpa_supplicant_file:
        wpa_supplicant_file.write(wpa_replace)

def is_online():
    conn = httplib.HTTPConnection("www.google.com", timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except:
        conn.close()
        return False

# 
# use 'iw scan' command to scan network.
# 
# ap-force option allows to scan when in Access Point mode.
# 
# skips duplicates based on SSID
#
def scan(test_string=None):
    if device != "wlan0":
        print("use 'iwlist' command to scan network with %s" % device)
        return list_scan()

    print("use 'iw' command to scan network with %s" % device)
        
    current_sid = current()

    if test_string:
        iw = test_string
    else:
        iw = cmd('sudo iw dev %s scan ap-force | egrep "^BSS|SSID:|WPS:|WPA:|signal:"' % device)

    networks = iw.split("BSS ")
    networks.pop(0)

    info_list = []

    for network in networks:
        network = network.replace(" ", "").replace("\t","")
        
        net_lines = network.splitlines()

        info_dict = {"ADDRESS":net_lines.pop(0).split("(")[0]}

        for info in net_lines:
            info = info.split(':',1)
            info_dict[info[0]] = info[1]

        if "WPS" in info_dict:
            info_dict["WPA"] = info_dict["WPS"]

        if "signal" in info_dict:
            info_dict["signal"] = info_dict["signal"].replace("dBm","")

        if info_dict["SSID"] == current_sid:
            info_dict["CONNECTED"] = True
        else:
            info_dict["CONNECTED"] = False

        ignore = False
        for info_item in info_list:
            if info_item["SSID"] == info_dict["SSID"] or info_dict["SSID"] == "":
                ignore = True
        
        if not ignore:
            info_list.append(info_dict)

    print(info_list)
    print("found %d available wifi networks" % len(info_list))
    return info_list

#   
# use 'iwlist scan' command instead of 'iw scan' for debugging on ubuntu.
#
def list_scan():
    current_sid = current()
    
    iwlist = cmd("sudo iwlist %s scanning | egrep 'Cell |IEEE|ESSID'" % device) # wlp58s0
    networks = iwlist.split("Cell")
    networks.pop(0)
    info_list = []
    for network in networks:
        network = network.replace(" ", "")
        network = network.replace('"', '')
        net_lines = network.splitlines()

        net_lines[0] = net_lines[0].split("-",1)[1]

        info_dict = {}

        for info in net_lines:
            info = info.split(':',1)
            info_dict[info[0]] = info[1]

        if "IE" in info_dict:
            ie = info_dict["IE"].split("/")
            info_dict["IEEE"] = ie[0].replace("IEEE","")
            info_dict["WPA"] = ie[1]

        info_dict["SSID"] = info_dict.pop("ESSID")

        if info_dict["SSID"] == current_sid:
            info_dict["CONNECTED"] = True
        else:
            info_dict["CONNECTED"] = False

        ignore = False
        for info_item in info_list:
            if info_item["SSID"] == info_dict["SSID"] or info_dict["SSID"] == "":
                ignore = True
        
        if not ignore:
            info_list.append(info_dict)

    print("found %d available wifi networks" % len(info_list))
    return info_list

#
# use 'iwgetid' to find current wifi connection ssid
#
def current():
    iwgetid = cmd("iwgetid")
    iwgetid = iwgetid.split("ESSID:")
    if len(iwgetid) > 1:
        essid = iwgetid[1].replace('"','').replace('\n','').replace(' ','')
        print("connected to %s" % essid)
        return essid
    else:
        print("Wifi not connected")
        return ""

def get_status():
    connection = current()
    online = is_online()
    return {"ssid":connection, "online":online}

def cmd(arg):
    ret = os.popen(arg).read()
    return ret

if __name__ == "__main__":
    networks = scan('''BSS e0:28:6d:0c:ff:40(on wlan0)
	signal: -60.00 dBm
	SSID: spaceboy
	WPS:	 * Version: 1.0
BSS 8a:5c:44:c6:2f:93(on wlan0)
	signal: -78.00 dBm
	SSID: Vodafone Homespot
BSS 90:5c:44:c6:2f:93(on wlan0)
	signal: -76.00 dBm
	SSID: KabelBox-C8E6
	WPS:	 * Version: 1.0
BSS 4c:12:65:be:1c:3f(on wlan0)
	signal: -82.00 dBm
	SSID: Vodafone-E6D9
	WPS:	 * Version: 1.0
BSS 00:e0:20:1c:3e:e8(on wlan0)
	signal: -60.00 dBm
	SSID: Spl@SH_Ext
	WPS:	 * Version: 1.0
BSS 54:67:51:4d:df:e1(on wlan0)
	signal: -84.00 dBm
	SSID: KabelBox-C71C
	WPS:	 * Version: 1.0
BSS d4:63:fe:51:c0:f7(on wlan0)
	signal: -74.00 dBm
	SSID: o2-WLAN02
	WPS:	 * Version: 1.0
BSS 86:5c:44:c6:2f:93(on wlan0)
	signal: -80.00 dBm
	SSID: Vodafone Hotspot
BSS 94:6a:b0:4c:f1:28(on wlan0)
	signal: -88.00 dBm
	SSID: WLAN-119901
	WPS:	 * Version: 1.0
BSS 44:4e:6d:24:3c:2d(on wlan0)
	signal: -70.00 dBm
	SSID: CDRWLAN
	WPS:	 * Version: 1.0
BSS 0c:47:3d:3d:e4:4a(on wlan0)
	signal: -82.00 dBm
	SSID: Vodafone Hotspot''')

    for n in networks:
        print(n)